FROM gradle:4.7-jdk8 as build
USER root
WORKDIR /home/gradle/project
COPY . .
RUN chown -R gradle:gradle /home/gradle/
USER gradle
RUN gradle build
CMD gradle run

EXPOSE 8080