@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")

package headers

import kotlin.js.RegExp

external interface MustacheScanner {
    var string: String
    var tail: String
    var pos: Number
    fun eos(): Boolean
    fun scan(re: RegExp): String
    fun scanUntil(re: RegExp): String
}

external interface MustacheContext {
    var view: Any
    var parentContext: MustacheContext
    fun push(view: Any): MustacheContext
    fun lookup(name: String): Any
}

external interface MustacheWriter {
    @nativeInvoke
    operator fun invoke(view: Any): String

    fun clearCache()
    fun parse(template: String, tags: Any? = definedExternally /* null */): Any
    fun render(template: String, view: Any, partials: Any): String
    fun renderTokens(tokens: Array<String>, context: MustacheContext, partials: Any, originalTemplate: Any): String
}

external interface MustacheStatic {
    var name: String
    var version: String
    var tags: String
    var Scanner: MustacheScanner
    var Context: MustacheContext
    var Writer: MustacheWriter
    var escape: Any
    fun clearCache(): MustacheWriter
    fun parse(template: String, tags: Any? = definedExternally /* null */): Any
    fun render(template: String, view: Any, partials: Any? = definedExternally /* null */): String
    fun to_html(template: String, view: Any, partials: Any? = definedExternally /* null */, send: Any? = definedExternally /* null */): Any
}

//@JsModule("mustache")
external val Mustache: MustacheStatic = definedExternally
