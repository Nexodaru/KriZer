@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")
@file:JsQualifier("JQueryValidation")

package headers.jqueryValidation

import JQuery
import JQueryEventObject
import org.w3c.dom.Element
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLFormElement

external interface RulesDictionary {
    @nativeGetter
    operator fun get(name: String): Any?

    @nativeSetter
    operator fun set(name: String, value: Any)
}

external interface `T$0` {
    @nativeGetter
    operator fun get(groupName: String): String?

    @nativeSetter
    operator fun set(groupName: String, value: String)
}

external interface ValidationOptions {
    var debug: Boolean? get() = definedExternally; set(value) = definedExternally
    var errorClass: String? get() = definedExternally; set(value) = definedExternally
    var errorContainer: String? get() = definedExternally; set(value) = definedExternally
    var errorElement: String? get() = definedExternally; set(value) = definedExternally
    var errorLabelContainer: String? get() = definedExternally; set(value) = definedExternally
    val errorPlacement: ((error: JQuery, element: JQuery) -> Unit)? get() = definedExternally
    var focusCleanup: Boolean? get() = definedExternally; set(value) = definedExternally
    var focusInvalid: Boolean? get() = definedExternally; set(value) = definedExternally
    var groups: `T$0`? get() = definedExternally; set(value) = definedExternally
    val highlight: ((element: HTMLElement, errorClass: String, validClass: String) -> Unit)? get() = definedExternally
    var ignore: String? get() = definedExternally; set(value) = definedExternally
    var ignoreTitle: Boolean? get() = definedExternally; set(value) = definedExternally
    val invalidHandler: ((event: JQueryEventObject, validator: Validator) -> Unit)? get() = definedExternally
    var messages: Any? get() = definedExternally; set(value) = definedExternally
    var meta: String? get() = definedExternally; set(value) = definedExternally
    var onclick: dynamic /* Boolean | (element: HTMLElement, event: JQueryEventObject) -> Unit */ get() = definedExternally; set(value) = definedExternally
    var onfocusin: ((element: HTMLElement, event: JQueryEventObject) -> Unit)? get() = definedExternally; set(value) = definedExternally
    var onfocusout: dynamic /* Boolean | (element: HTMLElement, event: JQueryEventObject) -> Unit */ get() = definedExternally; set(value) = definedExternally
    var onkeyup: dynamic /* Boolean | (element: HTMLElement, event: JQueryEventObject) -> Unit */ get() = definedExternally; set(value) = definedExternally
    var onsubmit: Boolean? get() = definedExternally; set(value) = definedExternally
    var rules: RulesDictionary? get() = definedExternally; set(value) = definedExternally
    val showErrors: ((errorMap: ErrorDictionary, errorList: Array<ErrorListItem>) -> Unit)? get() = definedExternally
    val submitHandler: ((form: HTMLFormElement, event: JQueryEventObject? /*= null*/) -> Unit)? get() = definedExternally
    var success: dynamic /* String | (`$label`: headers.JQuery, validatedInput: HTMLElement) -> Unit */ get() = definedExternally; set(value) = definedExternally
    val unhighlight: ((element: HTMLElement, errorClass: String, validClass: String) -> Unit)? get() = definedExternally
    var validClass: String? get() = definedExternally; set(value) = definedExternally
    var wrapper: String? get() = definedExternally; set(value) = definedExternally
}

external interface ErrorDictionary {
    @nativeGetter
    operator fun get(name: String): String?

    @nativeSetter
    operator fun set(name: String, value: String)
}

external interface ErrorListItem {
    var message: String
    var element: HTMLElement
}

external interface `T$1` {
    @nativeGetter
    operator fun get(index: String): Function<*>?

    @nativeSetter
    operator fun set(index: String, value: Function<*>)
}

external interface ValidatorStatic {
    fun addClassRules(name: String, rules: RulesDictionary)
    fun addClassRules(rules: RulesDictionary)
    fun addMethod(name: String, method: (value: Any, element: HTMLElement, params: Any) -> Boolean, message: String? = definedExternally /* null */)
    fun addMethod(name: String, method: (value: Any, element: HTMLElement, params: Any) -> Boolean, message: ((params: Any, element: HTMLElement) -> String)? = definedExternally /* null */)
    fun format(template: String): (args: Any) -> String
    fun format(template: String, vararg args: Any): String
    fun setDefaults(defaults: ValidationOptions)
    var messages: `T$0`
    var methods: `T$1`
    fun addMethod(name: String, method: (value: Any, element: HTMLElement, params: Any) -> Boolean)
}

external interface Validator {
    fun element(element: String): Boolean
    fun element(element: JQuery): Boolean
    fun checkForm(): Boolean
    fun form(): Boolean
    fun elementValue(element: Element): Any
    fun invalidElements(): Array<HTMLElement>
    fun numberOfInvalids(): Number
    fun resetForm()
    var settings: ValidationOptions
    fun showErrors(errors: Any)
    fun hideErrors()
    fun valid(): Boolean
    fun validElements(): Array<HTMLElement>
    fun size(): Number
    fun focusInvalid()
    var errorMap: ErrorDictionary
    var errorList: Array<ErrorListItem>
}
