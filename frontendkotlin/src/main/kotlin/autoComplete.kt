import kotlinx.html.InputType
import kotlinx.html.input
import kotlinx.html.stream.createHTML
import kotlinx.html.strong
import org.w3c.dom.*
import org.w3c.dom.events.Event
import org.w3c.dom.events.KeyboardEvent
import kotlin.browser.document



fun autoComplete(inputElement: HTMLInputElement, list: Array<String>) {
    var currentFocus = -1

    fun addActive(elements: HTMLCollection?) {
        println("addActiv")
        if (elements == null) return
        removeActive(elements)
        if (currentFocus >= elements.length) currentFocus = 0
        if (currentFocus < 0) currentFocus = (elements.length - 1)
        elements[currentFocus]?.classList?.add("autocomplete-active")
    }

    inputElement.addEventListener("input", {
        //Add Parent Classes to fit in Size
        val parentDOMTokenList = inputElement.parentElement?.classList
        var parentClasses = ""
        for (i in 0 until (parentDOMTokenList?.length ?: 0)) {
            val actClass = parentDOMTokenList?.get(i)
            if (actClass != "autocomplete") actClass?.let { it1 -> parentClasses += " $it1" }
        }

        val value = inputElement.value

        closeAllLists()
        if (value.isEmpty()) return@addEventListener
        currentFocus = -1

        val autoCompleteListDIV = document.createElement("DIV")
        autoCompleteListDIV.setAttribute("id", inputElement.id + "autocomplete-list")
        autoCompleteListDIV.setAttribute("class", "autocomplete-items $parentClasses")

        inputElement.parentNode?.appendChild(autoCompleteListDIV)

        for (i in 0 until list.size) {
            if (list[i].substring(0, value.length).toUpperCase() == value.toUpperCase()) {
                val b = document.createElement("DIV")
                b.setAttribute("class", parentClasses)
                b.innerHTML = createHTML().strong {
                    +list[i].substring(0, value.length)
                }
                b.innerHTML += list[i].substring(value.length)
                b.innerHTML += createHTML().input {
                    type = InputType.hidden
                    this.value = list[i]
                }
                b.addEventListener("click", {
                    inputElement.value = (b.getElementsByTagName("input")[0] as HTMLInputElement).value
                    closeAllLists()
                })
                autoCompleteListDIV.appendChild(b)
            }
        }
    })

    inputElement.addEventListener("keydown", fun(event: Event) {
        val autoCompleteListDIV = document.getElementById(inputElement.id + "autocomplete-list") as? HTMLDivElement
        val autoCompleteListElements = autoCompleteListDIV?.getElementsByTagName("div")
        val keyboardEvent = event as KeyboardEvent
        when (keyboardEvent.keyCode) {
            40 -> {
                println("40")
                currentFocus++
                addActive(autoCompleteListElements)
            }
            38 -> {
                println("38")
                currentFocus--
                addActive(autoCompleteListElements)
            }
            13 -> {
                event.preventDefault()
                if (currentFocus > -1) {
                    (autoCompleteListElements?.get(currentFocus) as HTMLDivElement).click()
                }
            }
        }
    })
    document.addEventListener("click", fun(event: Event) {
        closeAllLists(event.target as HTMLElement)
    })
}

private fun closeAllLists(element: HTMLElement? = null) {
    println("closeAll")
    val items = document.getElementsByClassName("autocomplete-items")
    for (i in 0 until items.length) {
        if (element != items.get(i) as HTMLElement) {
            items.get(i)?.parentNode?.removeChild(items.get(i) as HTMLElement)
        }
    }
}

private fun removeActive(elements: HTMLCollection) {
    println("remove")
    for (i in 0 until elements.length) {
        elements.get(i)?.classList?.remove("autocomplete-active")
    }
}
