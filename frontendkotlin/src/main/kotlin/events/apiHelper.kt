package events

import autoComplete
import org.w3c.dom.HTMLInputElement
import org.w3c.xhr.XMLHttpRequest
import kotlin.browser.document


fun setAutoCompleteByGet(uri: String, id: String) {
    val http = XMLHttpRequest()
    http.open("get", uri, true);
    http.onreadystatechange = {
        val readyState = http.readyState
        if (readyState == XMLHttpRequest.DONE) {
            if (http.status == 200.toShort()) {
                val itemList = JSON.parse<Array<String>>(http.responseText)
                autoComplete(document.getElementById(id) as HTMLInputElement, itemList)
            }
        }
    }
    http.send()
}

