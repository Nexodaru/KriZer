package events

import API_EP_EVENT_LIST_FULL
import EVENT_CARD_TEMPLATE
import appendLoaderRow
import endOfDay
import getDatepickerDate
import getRootUrl
import headers.Mustache
import jQuery
import kotlinx.html.dom.append
import kotlinx.html.js.div
import org.w3c.dom.HTMLDivElement
import removeChilds
import replaceEntry

fun search() {
    val eventForm = jQuery("form[name='eventForm']")
    eventForm.submit()
    if (!eventForm.valid()) return

    val target = jQuery("#eventTarget")[0] as? HTMLDivElement
    target?.removeChilds()
    target?.appendLoaderRow()

    val startDateField = jQuery("#inputStartDate")
    val startDate = startDateField.getDatepickerDate()
    val endDateField = jQuery("#inputEndDate")
    val endDate = endDateField.getDatepickerDate()?.endOfDay()
    val formArray = eventForm.serializeArray()
    formArray.replaceEntry(startDateField.attr("name"), startDate?.toISOString() ?: "")
    formArray.replaceEntry(endDateField.attr("name"), endDate?.toISOString() ?: "")

    val data = jQuery.param(formArray)

    jQuery.get(getRootUrl() + EVENT_CARD_TEMPLATE, { template, textStatus, jqXHR ->
        jQuery.post(getRootUrl() + API_EP_EVENT_LIST_FULL, data, { eventArray: Any, _, _ ->
            jQuery("#refreshLoader").remove()
            val events = eventArray as Array<*>
            if (events.isEmpty()) target?.appendNotFound()
            for (i in events) {
                target?.appendEventCard(template as String, i!!)
            }
        }, "json")
    }, "text")
}

private fun HTMLDivElement.appendNotFound() {
    append {
        div("row") {
            div("col-auto d-flex mx-auto align-items-center") {
                +"Es wurden keine Events gefunden!"
            }
        }
    }
}

private fun HTMLDivElement.appendEventCard(template: String, event: Any) {
    val rendered = Mustache.render(template, event)
    insertAdjacentHTML("beforeend", rendered);
}

/*fun showModal(id:String){
    jQuery("#$id").asDynamic().modal("show")
}*/
