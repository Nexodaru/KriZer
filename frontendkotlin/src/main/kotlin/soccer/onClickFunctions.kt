package soccer

import API_SOCCER_AVERAGE
import API_SOCCER_QUESTION_1
import ddmmyyyy
import getDatepickerDate
import getRootUrl
import jQuery
import kotlinx.html.dom.append
import kotlinx.html.p
import org.w3c.dom.HTMLDivElement
import org.w3c.xhr.XMLHttpRequest
import removeChilds
import replaceEntry
import kotlin.browser.document
import kotlin.browser.window
import kotlin.dom.addClass
import kotlin.dom.removeClass
import JQuerySerializeArrayElement

fun answer1() {
    val form = jQuery("form[name='question1']")
    form.submit()
    if (!form.valid()) return

    val responseWrapper = jQuery("#responseWrapper1")[0] as? HTMLDivElement
    responseWrapper?.removeChilds()

    val formArray = form.serializeArray()
    val startDate = form.find("input[name='startDate']").getDatepickerDate()
    val endDate = form.find("input[name='endDate']").getDatepickerDate()

    formArray.replaceEntry("startDate", startDate?.toISOString() ?: "")
    formArray.replaceEntry("endDate", endDate?.toISOString() ?: "")
    val formdata = jQuery.param(formArray)
    jQuery.post(getRootUrl() + API_SOCCER_QUESTION_1, formdata, { answer, _, _ ->

        console.log(answer)
        val sumBeer = (answer.asDynamic()["sumBeer"] as? Int)?:0

        val sumMarriage = (answer.asDynamic()["sumMarriage"] as? Int)?:0
        val sumMatches = (answer.asDynamic()["sumMatches"] as? Int)?: 0
        val staat = answer.asDynamic()["state"] as? String
        jQuery.post(getRootUrl()+API_SOCCER_AVERAGE, "statedb=$staat", { answerAverage, _, _ ->
            val avgBeer = (answerAverage.asDynamic()["avBeer"] as? Int)?: 0
            val avgMarr = (answerAverage.asDynamic()["avMarriage"] as? Int) ?:0
            val deltaBeer = avgBeer-sumBeer
            val deltaMarr = avgMarr-sumMarriage
            responseWrapper?.append {
                p {
                    +"Im Zeitraum ${startDate?.ddmmyyyy()} bis ${endDate?.ddmmyyyy()} fanden $sumMatches"
                    +" Spiele der Mannschaft ${formArray.first { it.name == "teamName" }.value} statt. Dabei wurde in deren Heimatbundesstaat $staat"
                    +" im Monat durchschnittlich $deltaBeer hl ${if (deltaBeer<0) "weniger" else "mehr"} Bier getrunken"
                    +" und $deltaMarr mal ${if (deltaMarr<0) "weniger" else "mehr"} geheiratet!"
                }
            } ?: ""
        })
    })
}

fun updateDatasets() {
    val loader = document.getElementById("refreshLoader")
    loader?.className = ""
    loader?.addClass("loader")
    loader?.setAttribute("style", "visibility: visible")
    val http = XMLHttpRequest()
    http.open("post", window.location.href, true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.onreadystatechange = {
        val readyState = http.readyState
        if (readyState == XMLHttpRequest.DONE) {
            if (http.status == 200.toShort()) {
                loader?.removeClass("loader")
                loader?.addClass("fas fa-check fa-2x")
            } else {
                loader?.removeClass("loader")
                loader?.addClass("fas fa-times fa-2x")
                window.alert("Der TripleStore scheint gerade nicht verfügbar zu sein!\nVersuche es später nocheinmal.")
                throw Exception("")
            }
        }
    }
    http.send("do=refreshRDF")
}