import kotlinx.html.div
import kotlinx.html.dom.create
import kotlinx.html.id
import kotlinx.html.js.div
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement
import org.w3c.dom.Node
import kotlin.browser.document
import kotlin.js.Date

fun getRootUrl(): String {
    return js("window.location.origin?window.location.origin+'/':window.location.protocol+'/'+window.location.host+'/'") as String
}

fun HTMLDivElement.appendLoaderRow() {
    val loaderRow = document.create.div("row") {
        div("col-auto d-flex mx-auto align-items-center") {
            div("loader") {
                id = "refreshLoader"
                attributes["style"] = "visibility: visible"
            }
        }
    }
    append(loaderRow)
}

fun HTMLElement.removeChilds() {
    while (lastChild != null) {
        removeChild(lastChild as Node);
    }
}

fun Array<JQuerySerializeArrayElement>.replaceEntry(name: String, value: String) {
    forEachIndexed { i, entry ->
        if (entry.name == name) get(i).value = value
    }
}

fun JQuery.getDatepickerDate(): Date? {
    return asDynamic().datepicker("getUTCDate") as? Date
}

fun Date.endOfDay(): Date {
    return Date(getUTCFullYear(), getUTCMonth(), getUTCDay(), 25, 59, 59)
}

fun Date.ddmmyyyy(): String {
    return "${getUTCDate()}/${getUTCMonth() + 1}/${getUTCFullYear()}"
}
