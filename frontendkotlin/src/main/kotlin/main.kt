import events.setAutoCompleteByGet
import org.w3c.dom.HTMLButtonElement
import org.w3c.dom.HTMLImageElement
import org.w3c.dom.get
import kotlin.browser.document

fun main(args: Array<String>) {
    val currentPageTag = jQuery("meta[name='tag']").attr("content")
    val navAvatar = document.getElementById("navAvatar") as? HTMLImageElement
    when (currentPageTag) {
        "soccer" -> {
            navAvatar?.src = "/static/circle_soccer_80.png"

        }
        "events" -> {
            navAvatar?.src = "/static/circle_events_80.png"
            setAutoCompleteByGet(getRootUrl() + API_EP_ARTIST_LIST, "artistInput")
            setAutoCompleteByGet(getRootUrl() + API_EP_CITY_LIST, "cityInput")

            jQuery("#detailsModal").on("show.bs.modal", fun(event: JQueryEventObject, args: Any) {
                val button = event.relatedTarget as HTMLButtonElement
                val eid = button.dataset["eid"]

                jQuery.get(getRootUrl() + API_EP_EVENT + "/$eid", { response, _, _ ->
                    val event = response.asDynamic()
                    console.log(event)
                    val address = "${event.adress}, ${event.zip} ${event.city}, ${event.state}, ${event.country}"
                    val modal = jQuery("#detailsModal")
                    modal.find(".modal-title").text(event.eventName as String)
                    modal.find(".modal-body #artist-name").text(event.artistName as String)
                    modal.find(".modal-body #artist-id").text(event.mbid as String)
                    modal.find(".modal-body #artist-type").text(event.artistType as String)
                    modal.find(".modal-body #artist-bio").text(event.bio as String)
                    modal.find(".modal-body #event-name").text(event.eventName as String)
                    modal.find(".modal-body #event-start").text(event.eventStart as String)
                    modal.find(".modal-body #event-desc").text(event.eventDesc as String)
                    modal.find(".modal-body #event-id").text(event.eid as String)
                    modal.find(".modal-body #event-info").text("${event.info}")
                    modal.find(".modal-body #location-name").text(event.venueName as String)
                    modal.find(".modal-body #location-typ").text(event.venueType as String)
                    modal.find(".modal-body #location").text(address)
                    modal.find(".modal-body #location-coords").text("${event.lat}, ${event.long}")

                })

            })
        }
    }
}

