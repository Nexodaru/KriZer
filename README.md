# KriZler

Projekt für SemanticWeb und Betriebliche Informationssysteme an der HTWK.

## Getting Started


### Prerequisites

__1. Setzen von JAVA_HOME__

Um den Gradle Wrapper zu nutzen muss die Umgebungsvariable JAVA_HOME auf den Pfad des JDKs gesetzt sein (jdkFolder/bin).

## Running a local development server

Um die App Local mit Gradle zu bauen und danach zu starten, ist im Root-Verzeichnis des Projektes

```
$ gradlew run
```

auszuführen. Oder aber bei Intellij Idea unter 

```
Settings -> Build, Execution, Deployment -> Gradle
```

die Option

```
Use Gradle Wrapper Task
```

auszuwählen. Danach genügt ein ausführen der main mit Intellij.

Weiß nicht ob das Production Ready ist, aber es ist so erstmal einfach mit docker umzusetzen. 


## Running the tests and the shell


### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [ktor]( https://github.com/ktorio/ktor) - Simple Kotlin WebFramework
* [Apache Jena](https://jena.apache.org/) - Framework to manage TripleStore Stuff xD

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Franz Schwarzer** - *Initial work* - [Nexodaru](https://gitlab.com/Nexodaru)

See also the list of [contributors](https://gitlab.com/Nexodaru/KriZer/project_members) who participated in this project.

## License

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

