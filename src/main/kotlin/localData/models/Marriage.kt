package localData.models

import parseMonthWordtoInt
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.time.LocalDate
import java.time.YearMonth
import java.util.*

/**
 * Model for Marriage.
 * Represent count of Marriages in specific [startDate] to [endDate], [state]
 *
 * @property startDate start of Period when Marriages take place
 * @property endDate end of period when Marriage take place
 * @property state State where Marriage take place
 * @property countMarriage Number of Marriages in [startDate] to [endDate], [state]
 * @constructor Create an new MarriageObject with [startDate], [endDate], [state], [countMarriage]
 */
class Marriage(var startDate: LocalDate, var endDate: LocalDate, var state: String, var countMarriage: Int) {


    companion object {
        /**
         * Create a ArrayList of [Marriage] from csv in Path [resPath].
         * @param resPath Relativ Path to csv File (Format: from https://www-genesis.destatis.de)
         * @return ArrayList of [Marriage]
         */
        fun getMarriageListFromCsv(resPath: String): ArrayList<Marriage> {
            var marriageList = ArrayList<Marriage>()
            try {
                var lineList = mutableListOf<String>()
                val inputStream = ClassLoader.getSystemResourceAsStream(resPath)
                BufferedReader(InputStreamReader(inputStream)).useLines { lines -> lines.drop(4).forEach { lineList.add(it) } }
                inputStream.close()
                lineList = lineList.dropLastWhile { !it.matches(Regex("^[0-9]{4}.*")) }.toMutableList()

                val heads = lineList.first().split(";").drop(2)

                lineList.drop(1).forEach { line ->
                    var columns = line.split(";")
                    val year = columns[0]
                    val month = columns[1]
                    columns = columns.drop(2)
                    for (i in 0 until heads.size) {
                        if (!columns[i].matches(Regex("^[0-9]+\$"))) continue
                        val countMarriage = Integer.parseInt(columns[i])
                        val yearMonth = YearMonth.of(Integer.parseInt(year), parseMonthWordtoInt(month))
                        val endDate = yearMonth.atEndOfMonth()
                        val startDate = yearMonth.atDay(1)
                        marriageList.add(Marriage(startDate, endDate, heads[i], countMarriage))
                    }
                }

            } catch (e: Exception) {
                println("Reading CSV Error!")
                e.printStackTrace()
            }
            return marriageList
        }
    }

    override fun toString(): String {
        return "Marriage(startDate=$startDate, endDate=$endDate, state='$state', countMarriage=$countMarriage)"
    }


}