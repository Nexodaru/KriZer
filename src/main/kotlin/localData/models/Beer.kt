package localData.models

import parseMonthWordtoInt
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.time.LocalDate
import java.time.YearMonth
import java.util.ArrayList

class Beer(var startDate: LocalDate, var endDate: LocalDate, var state: String, var beerConsume: Int) {

    companion object {

        fun getBeerList(resPath: String): ArrayList<Beer> {
            var beerList = ArrayList<Beer>()
            try {

                var lineList = mutableListOf<String>()
                val inputStream = ClassLoader.getSystemResourceAsStream(resPath)
                BufferedReader(InputStreamReader(inputStream)).useLines { lines -> lines.drop(4).forEach { lineList.add(it) } }
                inputStream.close()
                lineList = lineList.dropLastWhile { !it.matches(Regex("^[0-9]{4}.*")) }.toMutableList()

                val heads = lineList.first().split(";").drop(2)

                lineList.drop(1).forEach { line ->
                    var columns = line.split(";")
                    val year = columns[0]
                    val month = columns[1]
                    columns = columns.drop(2)
                    for (i in 0 until heads.size) {
                        if (!columns[i].matches(Regex("^[0-9]+\$"))) continue
                        //println(columns[i])
                        val beerConsume = Integer.parseInt(columns[i])
                        val yearMonth = YearMonth.of(Integer.parseInt(year), parseMonthWordtoInt(month))
                        val endDate = yearMonth.atEndOfMonth()
                        val startDate = yearMonth.atDay(1)
                        beerList.add(Beer(startDate, endDate, heads[i], beerConsume))
                    }
                }

            } catch (e: Exception) {
                println("Reading CSV Error!")
                e.printStackTrace()
            }
            return beerList
        }
    }
}