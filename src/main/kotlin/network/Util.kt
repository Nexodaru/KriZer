package network

import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import java.time.Duration
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit

class ApiRestrictionHelper(private val timeDelay: Duration) {
    private var lastQuerytime: LocalDateTime = LocalDateTime.MIN
    fun waitForQuery(): Boolean {
        val current = LocalDateTime.now()
        val readyTime = lastQuerytime.plus(timeDelay)
        if (current < readyTime) {
            val timeToWait = Duration.between(lastQuerytime, readyTime).toNanos()
            println("Waiting ... ( $timeToWait")
            runBlocking { delay(timeToWait, TimeUnit.NANOSECONDS) }
        } else {
            return false
        }

        return true

    }

    fun setLastQueryTime() {
        lastQuerytime = LocalDateTime.now()
    }
}