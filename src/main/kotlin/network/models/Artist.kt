package network.models

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import network.eventfulApi.EventfulLoader
import network.eventfulApi.LongEvent
import network.eventfulApi.getMatchedPerformer
import org.slf4j.LoggerFactory
import java.io.File

private val LOG = LoggerFactory.getLogger(Artist::class.java)

class Artist(@SerializedName("id") private val _mbID: String?,
             @SerializedName("type") private val _type: String?,
             @SerializedName("name") private val _name: String?,
             @SerializedName("country") private val _country: String?,
             @SerializedName("disambiguation") private val _disambiguation: String?
        //@SerializedName("similarArtists") private val _similarArtists: ArrayList<SimilarArtist>?
) {


    val mbID get() = _mbID ?: ""
    val type get() = _type ?: ""
    val name get() = _name ?: ""
    val country get() = _country ?: ""
    val disambiguation get() = _disambiguation ?: ""
    var shortBio = ""
        get() = field ?: ""
    var eventfulID = ""
        get() = field ?: ""


    var similarArtists: ArrayList<SimilarArtist>? = ArrayList()
        get() {
            if (field == null) field = ArrayList()
            return field
        }

    var events: ArrayList<LongEvent>? = ArrayList()
        get() {
            if (field == null) field = ArrayList()
            return field
        }

    override fun toString(): String {
        return "Artist(mbID=$mbID, type=$type, name=$name, country=$country, disambiguation=$disambiguation, similarArtists=$similarArtists, events=$events)"
    }

    fun addEventfulData(): Boolean {
        val searchedPerformer = EventfulLoader.searchPerformer(name)
        var matchedPerformer = searchedPerformer.getMatchedPerformer(name)

        if (matchedPerformer != null) {
            LOG.info("Passender Performer wurde gefunden: ${matchedPerformer}")
            matchedPerformer = EventfulLoader.getPerformer(matchedPerformer.id)

            eventfulID = matchedPerformer?.id ?: ""
            shortBio = matchedPerformer?.shortBio ?: ""
            val eventList = matchedPerformer?.events?.event
            LOG.info("Es wurden ${eventList?.size} für den Artisten $name gefunden!")

            eventList?.forEach {
                val event = EventfulLoader.getEvent(it.id)
                if (event != null) {
                    events?.add(event)
                }
            }
            return true
        } else {
            val founded = searchedPerformer.map { it.name }
            if (founded.isNotEmpty()) LOG.info("Artist wurde in Eventful nicht gefunden bzw wurde nicht gematched." +
                    "\nGesucht: $name Gefunden: $founded")
            return false
        }
    }


}

fun List<Artist>.writeJsonToFile(path: String) {
    val listType = object : TypeToken<List<Artist>>() {}.getType()
    val gson = Gson()
    val json = gson.toJson(this)
    File(path).writeText(json)
}

fun List<Artist>.setDataFromJsonFile(path: String): List<Artist> {

    val listType = object : TypeToken<List<Artist>>() {}.getType()
    val gson = Gson()
    val sb = StringBuilder()
    File(path).forEachLine { sb.append(it) }
    val artists: List<Artist> = gson.fromJson(sb.toString(), listType)
    return artists
}

fun List<Artist>.toMBIDToArtistMap(): Map<String, Artist> {
    return this.associateBy({ it.mbID }, { it })
}

class SimilarArtist(val mbid: String, val match: Double) {
    override fun toString(): String {
        return "SimilarArtist(mbid='$mbid', match=$match)"
    }
}