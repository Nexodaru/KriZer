package network.lastFMApi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.QueryMap

/*GET http://musicbrainz.org/ws/2/artist/?query=country:de&fmt=json
#&inc=aliases
Cache-Control: no-cache
User-Agent: KriZler( nexodaru@googlemail.com )
Content-Type: application/json*/

interface LastFMService {
    @Headers("Content-Type: application/json",
            "User-Agent: KriZler(nexodaru@googlemail.com)")
    @GET("2.0/")
    fun listSimilarArtists(@QueryMap queryMap: Map<String, String>): Call<SimilarResult>

    @Headers("Content-Type: application/json",
            "User-Agent: KriZler(nexodaru@googlemail.com)")
    @GET("2.0/")
    fun getCorrectedArtistName(@QueryMap queryMap: Map<String, String>): Call<CorrectionResult>

}