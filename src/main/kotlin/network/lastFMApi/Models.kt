package network.lastFMApi

import com.google.gson.annotations.SerializedName


class SimilarResult(@SerializedName("similarartists") private val _similarartists: SimilarArtists?) {
    val similarartists get() = _similarartists ?: SimilarArtists(emptyList())
    override fun toString(): String {
        return "SimilarResult(similarartists=$similarartists)"
    }
}

class SimilarArtists(@SerializedName("artist") private val _artist: List<Artist>?) {
    val artist get() = _artist ?: emptyList()
    override fun toString(): String {
        return "SimilarArtists(artist=$artist)"
    }
}

class Artist(@SerializedName("name") private val _name: String?,
             @SerializedName("mbid") private val _mbid: String?,
             var match: Double = 0.0) {
    val name get() = _name ?: ""
    val mbid get() = _mbid ?: ""
    override fun toString(): String {
        return "Artist(name='$name', mbid='$mbid', match=$match)"
    }
}

class CorrectionResult(val corrections: Corrections?)
class Corrections(val correction: Correction)
class Correction(val artist: Artist)