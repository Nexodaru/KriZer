package network.lastFMApi

import LASTFM_API_KEY
import LASTFM_DELAY
import LASTFM_SCROBBLER_BASE_URL
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.time.delay
import network.models.Artist
import network.models.SimilarArtist
import network.models.toMBIDToArtistMap
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.slf4j.LoggerFactory
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retry
import java.util.concurrent.TimeUnit



object LastFMDBLoader {
    private val LOG = LoggerFactory.getLogger(LastFMDBLoader::class.java)

    private val dispatcher = Dispatcher().apply { maxRequests = 1 }

    private val interceptor = Interceptor { chain ->
        runBlocking { delay(LASTFM_DELAY) }
        chain.proceed(chain.request())
    }
    private val client = OkHttpClient().newBuilder()
            .dispatcher(dispatcher)
            .addNetworkInterceptor(interceptor)
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS).build()

    private val gson = GsonBuilder()
            .create()
    private val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .baseUrl(LASTFM_SCROBBLER_BASE_URL)
            .build()
    private val service = retrofit.create(LastFMService::class.java)


    fun getSimilarArtistsByMIBD(mbid: String): List<network.lastFMApi.Artist> {
        val result = service.listSimilarArtists(getSimilarArtistsByMBIDQueryMap(mbid)).execute()
        val lastFMResult = result.body()
        if (result.isSuccessful && lastFMResult != null) {
            val similarArtists = if (lastFMResult.similarartists.artist.isNotEmpty()) {
                lastFMResult.similarartists.artist
            } else {
                emptyList()
            }
            return similarArtists
        }
        throw Exception(result.errorBody().toString())
    }


    private fun getSimilarArtistsByMBIDQueryMap(mbid: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map.put("method", "artist.getsimilar")
        map.put("format", "json")
        map.put("mbid", mbid)
        map.put("api_key", LASTFM_API_KEY)
        map.put("autocorrect", "1")
        return map
    }

    private fun getArtistNameCorrectionQueryMap(name: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map.put("method", "artist.getcorrection")
        map.put("format", "json")
        map.put("api_key", LASTFM_API_KEY)
        map.put("artist", name)
        return map
    }

    fun getArtistNameCorrection(name: String): String? {
        val result: Response<CorrectionResult>
        try {
            result = service.getCorrectedArtistName(getArtistNameCorrectionQueryMap(name)).execute()
        } catch (e: JsonSyntaxException) {
            LOG.info("Es wurde keine passender Artisten Name für $name bei LastFM gefunden")
            return null
        }
        val correctionResult = result.body()
        if (result.isSuccessful && correctionResult != null) {
            return correctionResult.corrections?.correction?.artist?.name
        } else {
            return null
        }
    }
}

fun List<Artist>.appendSimilarArtists(): List<Artist> {
    // Only add SimilarArtists which already in List, to avoid new Datasets of Artist outside Germany
    val artistsMap = this.toMBIDToArtistMap().filterKeys { it.isNotBlank() }
    var c = 0
    artistsMap.forEach { mbid, artist ->
        val similarArtists = retry(mbid) {
            LastFMDBLoader.getSimilarArtistsByMIBD(mbid)
        }
        similarArtists.filter { it.mbid.isNotEmpty() }.forEach { similarArtist ->
            if (artistsMap.containsKey(similarArtist.mbid)) {
                artist.similarArtists!!.add(SimilarArtist(similarArtist.mbid, similarArtist.match))
            }
        }
        c++
        println("$c ||${artistsMap.size}")
        println(artist.similarArtists)
        println(artist.events)
    }
    return artistsMap.values.toList()
}
