package network.musicBrainzApi

import network.models.ArtistResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.QueryMap

/*GET http://musicbrainz.org/ws/2/artist/?query=country:de&fmt=json
#&inc=aliases
Cache-Control: no-cache
User-Agent: KriZler( nexodaru@googlemail.com )
Content-Type: application/json*/

interface MusicBrainzService {
    @Headers("Content-Type: application/json",
            "User-Agent: KriZler(nexodaru@googlemail.com)"
    )
    @GET("artist")
    fun listGermanArtists(@QueryMap queryMap: Map<String, String>): Call<ArtistResult>
}