package network.musicBrainzApi

import MUSIC_BRAINZ_BASE_URL
import MUSIC_BRAINZ_DELAY
import MUSIC_BRAINZ_LIMIT
import com.google.gson.GsonBuilder
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.time.delay
import network.models.Artist
import network.models.ArtistResult
import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object MusicBrainzDBLoader {

    private val dispatcher = Dispatcher().apply { maxRequests = 1 }
    private val interceptor = Interceptor { chain ->
        runBlocking { delay(MUSIC_BRAINZ_DELAY) }
        chain.proceed(chain.request())
    }
    private val client = OkHttpClient().newBuilder()
            .dispatcher(dispatcher)
            .addNetworkInterceptor(interceptor)
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS).build()

    private val gson = GsonBuilder().create()
    private val retrofit = Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(MUSIC_BRAINZ_BASE_URL)
            .build()

    private val service = retrofit.create(MusicBrainzService::class.java)

    fun listGermanArtists(): List<Artist> {
        val artistList: ArrayList<Artist>
        var artistResult = queryGermanArtists(0)
        val resultCount = artistResult.count
        artistList = ArrayList(resultCount)
        artistList.addAll(artistResult.artists)
        for (i in MUSIC_BRAINZ_LIMIT..resultCount step MUSIC_BRAINZ_LIMIT) {
            artistResult = queryGermanArtists(i)
            artistList.addAll(artistResult.artists)
        }
        return artistList
    }

    fun list100GermanArtists(): ArrayList<Artist> {
        val artistList = ArrayList<Artist>()
        var artistResult = queryGermanArtists(0)
        artistList.addAll(artistResult.artists)
        return artistList
    }

    private fun queryGermanArtists(offset: Int): ArtistResult {
        val result = service.listGermanArtists(getGermanArtistsQueryMap(offset)).execute()
        val artistResult = result.body()
        if (result.isSuccessful && artistResult != null) {
            return artistResult
        }
        throw Exception(result.errorBody().toString())


    }

    private fun getGermanArtistsQueryMap(offset: Int): HashMap<String, String> {
        val map = HashMap<String, String>()
        map.put("query", "country:de")
        map.put("fmt", "json")
        map.put("offset", offset.toString())
        map.put("limit", MUSIC_BRAINZ_LIMIT.toString())
        return map
    }
}