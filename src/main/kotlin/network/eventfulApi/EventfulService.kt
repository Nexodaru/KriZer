package network.eventfulApi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.QueryMap

interface EventfulService {
    @Headers("Content-Type: application/json",
            "User-Agent: KriZler(nexodaru@googlemail.com)")
    @GET("json/performers/search")
    fun searchPerformer(@QueryMap queryMap: Map<String, String>): Call<PerformerSearchResult>

    @Headers("Content-Type: application/json",
            "User-Agent: KriZler(nexodaru@googlemail.com)")
    @GET("json/performers/get")
    fun getPerformer(@QueryMap queryMap: Map<String, String>): Call<Performer>

    @Headers("Content-Type: application/json",
            "User-Agent: KriZler(nexodaru@googlemail.com)")
    @GET("json/events/get")
    fun getEvent(@QueryMap queryMap: Map<String, String>): Call<LongEvent>
}
