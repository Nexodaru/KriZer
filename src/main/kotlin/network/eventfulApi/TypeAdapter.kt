package network.eventfulApi

import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter

class PerformerSearchResultTypeAdapter : TypeAdapter<PerformerSearchResult>() {
    val gson = Gson()

    override fun write(out: JsonWriter?, value: PerformerSearchResult?) {
        gson.toJson(value, PerformerSearchResult::class.java, out)
    }

    override fun read(`in`: JsonReader): PerformerSearchResult {
        `in`.beginObject()
        while (`in`.nextName() != "performers") {
            `in`.skipValue()
        }
        if (`in`.peek() == JsonToken.NULL) {
            `in`.skipValue()
            `in`.endObject()
            return PerformerSearchResult()
        }
        `in`.beginObject()
        while (`in`.nextName() != "performer") {
            `in`.skipValue()
        }
        val performerSearchResult = when (`in`.peek()) {
            JsonToken.BEGIN_ARRAY -> PerformerSearchResult(*gson.fromJson(`in`, Array<Performer>::class.java))
            JsonToken.BEGIN_OBJECT -> PerformerSearchResult(gson.fromJson(`in`, Performer::class.java))
            else -> throw JsonParseException("Unexpected token " + `in`.peek())
        }
        `in`.endObject()
        `in`.endObject()
        return performerSearchResult
    }
}