package network.eventfulApi

import EVENTFUL_API_KEY
import EVENTFUL_BASE_URI
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

object EventfulLoader {

    private val gson = GsonBuilder()
            .registerTypeAdapter(PerformerSearchResult::class.java, PerformerSearchResultTypeAdapter())
            .create()
    private val client = OkHttpClient().newBuilder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS).build()

    private val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .baseUrl(EVENTFUL_BASE_URI)
            .build()
    private val service = retrofit.create(EventfulService::class.java)

    fun searchPerformer(keywords: String): List<Performer> {
        val result = service.searchPerformer(getPerformerSearchQueryMap(keywords)).execute()
        val performerSearchResult = result.body()
        if (result.isSuccessful && performerSearchResult != null) {
            return performerSearchResult.performers
        }
        return emptyList()
    }

    fun getPerformer(id: String): Performer? {
        val result = service.getPerformer(getPerformerWithShortEventQueryMap(id)).execute()
        val performer = result.body()
        return if (result.isSuccessful && performer != null) {
            performer
        } else {
            println(result.errorBody())
            null
        }
    }

    fun getEvent(id: String): LongEvent? {
        val result = service.getEvent(getEventQueryMap(id)).execute()
        val event = result.body()
        return if (result.isSuccessful && event != null) {
            event
        } else {
            println(result.errorBody())
            null
        }

    }

    private fun isNameMatched(name1: String, name2: String) {

    }

    private fun getPerformerSearchQueryMap(keywords: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map.put("keywords", keywords)
        map.put("app_key", EVENTFUL_API_KEY)
        return map
    }

    private fun getPerformerWithShortEventQueryMap(id: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map.put("show_events", "true")
        map.put("id", id)
        map.put("app_key", EVENTFUL_API_KEY)
        return map
    }

    private fun getEventQueryMap(id: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map.put("id", id)
        map.put("app_key", EVENTFUL_API_KEY)
        return map
    }

}

