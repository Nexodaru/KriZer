package network.eventfulApi

import com.google.gson.annotations.SerializedName
import network.lastFMApi.LastFMDBLoader


class PerformerSearchResult(vararg perf: Performer) {
    var performers: List<Performer> = perf.asList()
}

class Performer(@SerializedName("id") val _id: String?,
                @SerializedName("name") val _name: String?,
                @SerializedName("events") val _events: Event?,
                @SerializedName("short_bio") val _shortBio: String?
) {
    val id get() = _id ?: ""
    val name get() = _name ?: ""
    val events get() = _events ?: Event(emptyList())
    val shortBio get() = _shortBio ?: ""

    override fun toString(): String {
        return "Performer(id=$id, name=$name, events=$events, shortBio=$shortBio)"
    }
}

class Event(val event: List<ShortEvent>) {
    override fun toString(): String {
        return "Event(event=$event)"
    }
}

fun List<Performer>.getMatchedPerformer(name: String): Performer? {
    val performerNameList = map { it.name }
    when (true) {
        isEmpty() -> return null
        name in performerNameList -> return first { it.name == name }
    }
    val corrName = LastFMDBLoader.getArtistNameCorrection(name)
    if (corrName in performerNameList) return first { it.name == corrName }
    val corrPerfList = map { Performer(it.id, LastFMDBLoader.getArtistNameCorrection(it.name), it.events, it.shortBio) }
    val corrNameList = corrPerfList.map { it.name }
    return when (true) {
        name in corrNameList || corrName in corrNameList -> corrPerfList.first { it.name == name || it.name == corrName }
        else -> null
    }
}

class ShortEvent(@SerializedName("location") private val _location: String?,
                 @SerializedName("start_time") private val _startTime: String?,
                 @SerializedName("stop_time") private val _stopTime: String?,
                 @SerializedName("url") private val _url: String?,
                 @SerializedName("title") private val _title: String?,
                 @SerializedName("id") private val _id: String?
) {
    val location get() = _location ?: ""
    val startTime get() = _startTime ?: ""
    val stopTime get() = _stopTime ?: ""
    val url get() = _url ?: ""
    val title get() = _title ?: ""
    val id get() = _id ?: ""

    override fun toString(): String {
        return "ShortEvent(_location=$_location, _startTime=$_startTime, _stopTime=$_stopTime, _url=$_url, _title=$_title, _id=$_id)"
    }


}

class LongEvent(@SerializedName("id") val _id: String?,
                @SerializedName("url") val _url: String?,
                @SerializedName("title") val _title: String?,
                @SerializedName("description") val _description: String?,
                @SerializedName("start_time") val _startTime: String?,
                @SerializedName("stop_time") val _stopTime: String?,
                @SerializedName("venue_name") val _venueName: String?,
                @SerializedName("venue_type") val _venueType: String?,
                @SerializedName("address") val _address: String?,
                @SerializedName("city") val _city: String?,
                @SerializedName("region") val _region: String?,
                @SerializedName("postal_code") val _postalCode: String?,
                @SerializedName("country") val _country: String?,
                @SerializedName("latitude") val _latitude: Double = 0.0,
                @SerializedName("longitude") val _longitude: Double = 0.0,
                @SerializedName("geocode_type") val _geocodeType: String?
) {
    val id get() = _id ?: ""
    val url get() = _url ?: ""
    val title get() = _title ?: ""
    val startTime get() = _startTime ?: ""
    val stopTime get() = _stopTime ?: ""
    val desc get() = _description ?: ""
    val venueName get() = _venueName ?: ""
    val venueType get() = _venueType ?: ""
    val address get() = _address ?: ""
    val city get() = _city ?: ""
    val region get() = _region ?: ""
    val postalCode get() = _postalCode ?: ""
    val country get() = _country ?: ""
    val geocodeType get() = _geocodeType ?: ""
    val lat get() = _latitude
    val long get() = _longitude

    override fun toString(): String {
        return "LongEvent(_id=$_id, _url=$_url, _title=$_title, _description=$_description, _startTime=$_startTime, _stopTime=$_stopTime, _venueName=$_venueName, _venueType=$_venueType, _address=$_address, _city=$_city, _region=$_region, _postalCode=$_postalCode, _country=$_country, _latitude=$_latitude, _longitude=$_longitude, _geocodeType=$_geocodeType)"
    }


}

/*
"location": "Berlin, BE",
"all_day": "0",
"stop_time": null,
"start_time": "2018-05-22 20:00:00",
"url": "http://berlin.eventful.com/events/arctic-monkeys-berlin-/E0-001-112516664-1?utm_source=apis&utm_medium=apim&utm_campaign=apic",
"title": "Arctic Monkeys Berlin",
"id": "E0-001-112516664-1",
"description": " Are you ready to welcome the best artist in the world to your city? If so, the time has come to purchase your Arctic Mo*/
