package network.openLigaApi

import com.google.gson.GsonBuilder
import network.openLigaApi.models.SoccerMatch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object OpenLigaDBLoader {

    fun listMatches(league: String, period: String): List<SoccerMatch>? {
        val gson = GsonBuilder().create()

        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://www.openligadb.de/")
                .build()

        val service = retrofit.create(OpenLigaService::class.java)
        val result = service.listMatches(league, period).execute()

        if (result.isSuccessful) {
            return result.body()?.map {
                if (it.team1.name == "Leverkusen") it.team1.name = "Bayer 04 Leverkusen"
                if (it.team2.name == "Leverkusen") it.team2.name = "Bayer 04 Leverkusen"
                it
            }
        } else {
            throw Exception("Retrofit Call was not successfully")
        }
    }
}