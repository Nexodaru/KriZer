package network.openLigaApi

import network.openLigaApi.models.SoccerMatch
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface OpenLigaService {


    @Headers("Content-Type: application/json")
    @GET("api/getmatchdata/{league}/{period}")
    fun listMatches(@Path("league") league: String, @Path("period") period: String): Call<List<SoccerMatch>>
}