package network.openLigaApi.models

import com.google.gson.annotations.SerializedName

class Team {
    @SerializedName("TeamName")
    var name = ""

    override fun toString(): String {
        return "Team(name='$name')"
    }


}