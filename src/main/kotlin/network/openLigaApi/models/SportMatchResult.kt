package network.openLigaApi.models

import com.google.gson.annotations.SerializedName

class SportMatchResult {

    @SerializedName("PointsTeam1")
    val pointsTeam1: Int = 0

    @SerializedName("PointsTeam2")
    val pointsTeam2: Int = 0

    override fun toString(): String {
        return "SportMatchResult(pointsTeam1=$pointsTeam1, pointsTeam2=$pointsTeam2)"
    }


}