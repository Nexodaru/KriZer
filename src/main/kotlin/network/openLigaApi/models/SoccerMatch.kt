package network.openLigaApi.models

import com.google.gson.annotations.SerializedName

class SoccerMatch {
    @SerializedName("MatchDateTimeUTC")
    lateinit var matchDateTimeUTC: String

    @SerializedName("Team1")
    lateinit var team1: Team

    @SerializedName("Team2")
    lateinit var team2: Team

    @SerializedName("MatchResults")
    lateinit var result: List<SportMatchResult>

    override fun toString(): String {
        return "SoccerMatch(matchDateTimeUTC='$matchDateTimeUTC', team1=$team1, team2=$team2, result=$result)"
    }


}