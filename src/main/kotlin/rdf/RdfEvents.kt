package rdf

import FUSEKI_BASE_URI
import FUSEKI_PORT
import network.models.Artist
import network.models.setDataFromJsonFile
import org.apache.jena.ontology.*
import org.apache.jena.rdf.model.ModelFactory


class RdfEvents : Rdf() {


    override val rdfTypName: String
        get() = "event"

    override val model: OntModel = ModelFactory.createOntologyModel()
    override val classes: MutableMap<Classes, OntClass> = HashMap()
    override val objectProperties: MutableMap<ObjectProperties, ObjectProperty> = HashMap()
    override val dataProperties: MutableMap<DataTypeProperties, DatatypeProperty> = HashMap()

    private lateinit var rfdTypName: String

    override fun createRDF() {
        var cc = 0
        init()
        val path = ClassLoader.getSystemResource("germanArtistsWithEvents").path
        val artists = ArrayList<Artist>().setDataFromJsonFile(path)
        val rdfArtistMap = HashMap<String, Individual>()

        artists.forEach { artist ->
            val rdfArtist = model.createIndividual("$FUSEKI_BASE_URI:$FUSEKI_PORT/" + artist.mbID, classes[Classes.MusicArtist])
            rdfArtist.addProperty(dataProperties[DataTypeProperties.artistType], artist.type)
            rdfArtist.addProperty(dataProperties[DataTypeProperties.Name], artist.name)
            rdfArtist.addProperty(dataProperties[DataTypeProperties.EID], artist.eventfulID)
            rdfArtist.addProperty(dataProperties[DataTypeProperties.MBID], artist.mbID)
            rdfArtist.addProperty(dataProperties[DataTypeProperties.biography], artist.shortBio)
            rdfArtist.addProperty(dataProperties[DataTypeProperties.country], artist.country)
            artist.events!!.forEach { event ->
                val rdfEvent = model.createIndividual(classes[Classes.Event])
                rdfEvent.addProperty(dataProperties[DataTypeProperties.Name], event.title)
                rdfEvent.addProperty(dataProperties[DataTypeProperties.EID], event.id)
                rdfEvent.addProperty(dataProperties[DataTypeProperties.description], event.desc)
                rdfEvent.addProperty(dataProperties[DataTypeProperties.infoUrl], event.url)
                rdfEvent.addProperty(dataProperties[DataTypeProperties.startDateTime], event.startTime.replace(" ", "T"))
                rdfEvent.addProperty(dataProperties[DataTypeProperties.endDateTime], event.stopTime.replace(" ", "T"))
                val rdfLocation = model.createIndividual(classes[Classes.Location])
                rdfLocation.addProperty(dataProperties[DataTypeProperties.country], event.country)
                rdfLocation.addProperty(dataProperties[DataTypeProperties.state], event.region)
                rdfLocation.addProperty(dataProperties[DataTypeProperties.adress], event.address)
                rdfLocation.addProperty(dataProperties[DataTypeProperties.city], event.city)
                rdfLocation.addProperty(dataProperties[DataTypeProperties.venueName], event.venueName)
                rdfLocation.addProperty(dataProperties[DataTypeProperties.venueType], event.venueType)
                rdfLocation.addProperty(dataProperties[DataTypeProperties.postalCode], event.postalCode)
                rdfLocation.addProperty(dataProperties[DataTypeProperties.lat], event.lat.toString())
                rdfLocation.addProperty(dataProperties[DataTypeProperties.long], event.long.toString())

                rdfEvent.addProperty(objectProperties[ObjectProperties.takePlace], rdfLocation)
                rdfArtist.addProperty(objectProperties[ObjectProperties.playOn], rdfEvent)
            }
            rdfArtistMap[artist.mbID] = rdfArtist
        }
        artists.forEach { artist ->
            artist.similarArtists!!.forEach { simArtist ->
                if (rdfArtistMap.containsKey(simArtist.mbid)) {
                    val rdfMatch = model.createIndividual(classes[Classes.Matching])
                    rdfMatch.addProperty(dataProperties[DataTypeProperties.percent], simArtist.match.toString())
                    rdfMatch.addProperty(objectProperties[ObjectProperties.matchedArtist], rdfArtistMap[simArtist.mbid])
                    rdfArtistMap[artist.mbID]!!.addProperty(objectProperties[ObjectProperties.similarArtist], rdfMatch)
                } else {
                    cc++
                }
            }
        }
        println("---------------------------- $cc -------------------")
    }
}

