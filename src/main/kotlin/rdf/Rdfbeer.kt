package rdf

import DBPEDIA_RES_BASE_URI
import localData.models.Beer
import localData.models.Marriage
import network.openLigaApi.OpenLigaDBLoader
import org.apache.jena.ontology.DatatypeProperty
import org.apache.jena.ontology.ObjectProperty
import org.apache.jena.ontology.OntClass
import org.apache.jena.ontology.OntModel
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.impl.ResourceImpl
import stateList


class Rdfbeer() : Rdf() {

    override val rdfTypName: String = "beer"
    override val model: OntModel = ModelFactory.createOntologyModel()
    override val classes: MutableMap<Classes, OntClass> = HashMap()
    override val objectProperties: MutableMap<ObjectProperties, ObjectProperty> = HashMap()
    override val dataProperties: MutableMap<DataTypeProperties, DatatypeProperty> = HashMap()

    override fun createRDF() {
        init()
        val marriageList = Marriage.getMarriageListFromCsv("Eheschliessungen.csv")
        val beerList = Beer.getBeerList("Bierdaten.csv")
        val matches = OpenLigaDBLoader.listMatches("el1718", "2017")
        if (matches != null) {
            matches.forEach {
                val rdfMatch = model.createIndividual(classes[Classes.Footballmatch])
                rdfMatch.addProperty(dataProperties[DataTypeProperties.date], it.matchDateTimeUTC)
                val rdfTeam1 = model.createIndividual(classes[Classes.Team1])
                val rdfTeam2 = model.createIndividual(classes[Classes.Team2])
                rdfTeam1.addProperty(dataProperties[DataTypeProperties.label], it.team1.name)
                rdfTeam2.addProperty(dataProperties[DataTypeProperties.label], it.team2.name)
                rdfTeam1.addProperty(dataProperties[DataTypeProperties.sameAs], ResourceImpl(DBPEDIA_RES_BASE_URI + it.team1.name.replace(" ", "_")))
                rdfTeam2.addProperty(dataProperties[DataTypeProperties.sameAs], ResourceImpl(DBPEDIA_RES_BASE_URI + it.team2.name.replace(" ", "_")))

                rdfMatch.addProperty(objectProperties[ObjectProperties.team], rdfTeam1)
                rdfMatch.addProperty(objectProperties[ObjectProperties.team], rdfTeam2)
            }
        }
        stateList.forEach {
            val rdfState = model.createIndividual(classes[Classes.State])
            rdfState.addProperty(dataProperties[DataTypeProperties.label], it)
            val filteredMarriageList = marriageList.filter { m -> m.state == it }
            val filteredBeerList = beerList.filter { m -> m.state == it }
            filteredMarriageList.forEach {
                val rdfMarriage = model.createIndividual(classes[Classes.Merry])
                rdfMarriage.addProperty(dataProperties[DataTypeProperties.startDate], it.startDate.atStartOfDay().toString())
                rdfMarriage.addProperty(dataProperties[DataTypeProperties.endDate], it.endDate.atStartOfDay().toString())
                rdfMarriage.addProperty(dataProperties[DataTypeProperties.marriages], it.countMarriage.toString())
                rdfState.addProperty(objectProperties[ObjectProperties.merry], rdfMarriage)
            }

            filteredBeerList.forEach {
                val rdfBeer = model.createIndividual(classes[Classes.Bier])
                rdfBeer.addProperty(dataProperties[DataTypeProperties.startDate], it.startDate.atStartOfDay().toString())
                rdfBeer.addProperty(dataProperties[DataTypeProperties.endDate], it.endDate.atStartOfDay().toString())
                rdfBeer.addProperty(dataProperties[DataTypeProperties.beersales], it.beerConsume.toString())
                rdfState.addProperty(objectProperties[ObjectProperties.beer], rdfBeer)
            }
        }
    }
}













