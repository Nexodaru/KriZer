package rdf

import FUSEKI_BASE_URI
import FUSEKI_DATASET_PATH
import FUSEKI_PORT
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.apache.jena.ontology.DatatypeProperty
import org.apache.jena.ontology.ObjectProperty
import org.apache.jena.ontology.OntClass
import org.apache.jena.ontology.OntModel
import org.apache.jena.rdfconnection.RDFConnectionFuseki
import java.io.FileOutputStream

abstract class Rdf {

    abstract val model: OntModel
    abstract val classes: MutableMap<Classes, OntClass>
    abstract val objectProperties: MutableMap<ObjectProperties, ObjectProperty>
    abstract val dataProperties: MutableMap<DataTypeProperties, DatatypeProperty>
    abstract val rdfTypName: String

    fun init() {

        for (value in Namespaces.values()) {
            model.setNsPrefix(value.name.toLowerCase(), value.url)
        }
        for (value in Classes.values()) {
            classes[value] = model.createClass(value.namespace.url + value.name)
        }
        for (value in ObjectProperties.values()) {
            val objectProperty = model.createObjectProperty(value.namespace.url + value.name)
            value.range.forEach { objectProperty.addRange(classes[it]) }
            value.domain.forEach { objectProperty.addDomain(classes[it]) }

            objectProperties[value] = objectProperty
        }
        for (value in DataTypeProperties.values()) {
            val datatypeProperty = model.createDatatypeProperty(value.namespace.url + value.name.toLowerCase())
            datatypeProperty.addRange(value.range)
            value.domain.forEach { datatypeProperty.addDomain(classes[it]) }

            dataProperties[value] = datatypeProperty
        }
    }

    abstract fun createRDF()

    fun write() {
        model.write(rdfTypName, FileFormats.RDF_XML_ABBREV)
        System.out.println("finished writing model")
    }

    fun OntModel.write(name: String, format: FileFormats) {
        write(FileOutputStream("src/main/resources/$name.rdf"), format.format)
    }

    fun send(memType: String) {
        val post = HttpPost("$FUSEKI_BASE_URI:$FUSEKI_PORT/$FUSEKI_DATASET_PATH")
        val urlParameters = ArrayList<BasicNameValuePair>()
        urlParameters.add(BasicNameValuePair("dbName", "/$rdfTypName"))
        urlParameters.add(BasicNameValuePair("dbType", memType))
        post.entity = UrlEncodedFormEntity(urlParameters)
        val httpclient = createHttpClientWithCreds()
        httpclient.execute(post)

        val conn = RDFConnectionFuseki.create().httpClient(httpclient)
                .destination("$FUSEKI_BASE_URI:$FUSEKI_PORT/$rdfTypName").build()

        //create new Dataset:
        //curl -v -u admin:pw123 -X POST -d "dbName=/dataseName&dbType=mem" http://localhost:3030/$/datasets

        conn.put(model)
        httpclient.close()
        conn.close()

    }

    fun deleteDataset() {
        val delete = HttpDelete("$FUSEKI_BASE_URI:$FUSEKI_PORT/$FUSEKI_DATASET_PATH/$rdfTypName")
        val httpClient = createHttpClientWithCreds()
        httpClient.execute(delete)
        httpClient.close()
    }

    fun refreshRDF() {
        val rdf = Rdfbeer()
        rdf.createRDF()
        rdf.deleteDataset()
        rdf.send("mem")
    }

    private fun createHttpClientWithCreds(): CloseableHttpClient {
        val credsProvider = BasicCredentialsProvider()
        val credentials = UsernamePasswordCredentials("admin", "pw123")
        credsProvider.setCredentials(AuthScope.ANY, credentials)
        return HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build()
    }
}