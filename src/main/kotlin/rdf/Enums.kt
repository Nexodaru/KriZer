package rdf

import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.XSD

enum class FileFormats(val format: String) {
    RDF_XML("RDF/XML"),
    RDF_XML_ABBREV("RDF/XML-ABBREV"),
    N_TRIPLE("N-TRIPLE"),
    TURTLE("TURTLE")
}

enum class Namespaces(val url: String) {

    XSD("http://www.w3.org/2001/XMLSchema#"),
    DBO("http://dbpedia.org/ontology/"),
    MKR("http://www.imn.htwk-leipzig.de/~mkriegs/"),
    FSC("http://www.imn.htwk-leipzig.de/~fschwarz/"),
    RDFS("http://www.w3.org/2000/01/rdf-schema#"),
    MO("http://purl.org/ontology/mo/"),
    FOAF("http://xmlns.com/foaf/0.1/"),
    OWL("http://www.w3.org/2002/07/owl#")
}

enum class Classes(val namespace: Namespaces) {
    //Micha
    Footballmatch(Namespaces.DBO),
    Team1(Namespaces.MKR),
    Team2(Namespaces.MKR),
    State(Namespaces.DBO),
    Bier(Namespaces.MKR),
    Merry(Namespaces.MKR),
    // Franz
    MusicArtist(Namespaces.MO),
    Location(Namespaces.DBO),
    Event(Namespaces.DBO),
    Matching(Namespaces.FSC)
}

enum class DataTypeProperties(val namespace: Namespaces, val range: Resource, vararg domains: Classes) {

    // Micha

    date(Namespaces.DBO, XSD.date, Classes.Footballmatch),

    label(Namespaces.RDFS, XSD.xstring, Classes.Team1, Classes.Team2, Classes.State),

    startDate(Namespaces.MKR, XSD.dateTime, Classes.Merry, Classes.Bier),

    endDate(Namespaces.MKR, XSD.dateTime, Classes.Merry, Classes.Bier),

    marriages(Namespaces.MKR, XSD.xint, Classes.Merry),

    staten(Namespaces.DBO, XSD.xstring, Classes.Team1),

    sameAs(Namespaces.OWL, XSD.anyURI, Classes.Team1, Classes.Team2),

    beersales(Namespaces.MKR, XSD.xint, Classes.Bier),

    //Franz

    biography(Namespaces.FSC, XSD.xstring, Classes.MusicArtist),

    EID(Namespaces.FSC, XSD.xstring, Classes.MusicArtist, Classes.Event),

    MBID(Namespaces.FSC, XSD.xstring, Classes.MusicArtist),

    description(Namespaces.FSC, XSD.xstring, Classes.Event),

    infoUrl(Namespaces.FSC, XSD.xstring, Classes.Event),

    venueName(Namespaces.FSC, XSD.xstring, Classes.Location),

    venueType(Namespaces.FSC, XSD.xstring, Classes.Location),

    city(Namespaces.FSC, XSD.xstring, Classes.Event),

    lat(Namespaces.FSC, XSD.xfloat, Classes.Event),

    long(Namespaces.FSC, XSD.xfloat, Classes.Event),

    Name(Namespaces.FOAF, XSD.xstring, Classes.MusicArtist, Classes.Event),

    country(Namespaces.DBO, XSD.xstring, Classes.MusicArtist, Classes.Location),

    artistType(Namespaces.FSC, XSD.xstring, Classes.MusicArtist),

    title(Namespaces.FSC, XSD.xstring, Classes.Event),

    startDateTime(Namespaces.FSC, XSD.dateTime, Classes.Event),

    endDateTime(Namespaces.FSC, XSD.dateTime, Classes.Event),

    state(Namespaces.DBO, XSD.xstring, Classes.Location),

    locationName(Namespaces.RDFS, XSD.xstring, Classes.Location),

    adress(Namespaces.DBO, XSD.xstring, Classes.Location),

    postalCode(Namespaces.FSC, XSD.xint, Classes.Location),

    percent(Namespaces.FSC, XSD.xdouble, Classes.Matching);

    val domain: Array<out Classes> = domains
}

enum class ObjectProperties(val namespace: Namespaces, val range: Array<Classes>, val domain: Array<Classes>) {

    //Micha
    merry(Namespaces.MKR, arrayOf(Classes.Merry), arrayOf(Classes.State)),
    release(Namespaces.MKR, arrayOf(Classes.Bier), arrayOf(Classes.State)),
    team(Namespaces.DBO, arrayOf(Classes.Team1, Classes.Team2), arrayOf(Classes.Footballmatch)),
    beer(Namespaces.MKR, arrayOf(Classes.Bier), arrayOf(Classes.State)),

    //Franz
    similarArtist(Namespaces.FSC, arrayOf(Classes.Matching), arrayOf(Classes.MusicArtist)),
    playOn(Namespaces.FSC, arrayOf(Classes.Event), arrayOf(Classes.MusicArtist)),
    takePlace(Namespaces.FSC, arrayOf(Classes.Location), arrayOf(Classes.Event)),
    matchedArtist(Namespaces.FSC, arrayOf(Classes.MusicArtist), arrayOf(Classes.Matching))

}
