package rdf

import FUSEKI_BASE_URI
import FUSEKI_ENDPOINT_EVENT
import FUSEKI_ENDPOINT_SOCCER
import FUSEKI_PORT
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.jena.query.QueryFactory
import org.apache.jena.query.ResultSet
import org.apache.jena.query.ResultSetFormatter
import org.apache.jena.query.Syntax
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdfconnection.RDFConnectionFuseki
import java.io.BufferedReader
import java.io.ByteArrayOutputStream
import java.io.InputStreamReader


object FusekiSparqlQueryHelper {

    private val builder = RDFConnectionFuseki.create()
            .destination("$FUSEKI_BASE_URI:$FUSEKI_PORT/")

    fun query(type: Query, ignoreMissingArgs: Boolean = false): ArrayList<Map<String, Literal>> {
        return query(type, emptyMap(), ignoreMissingArgs)
    }

    fun query(type: Query, parameter: Map<String, String>, ignoreMissingArgs: Boolean = false): ArrayList<Map<String, Literal>> {
        var httpClient = HttpClients.custom().build()
        when (type) {
            is EventQuery -> builder.httpClient(httpClient).queryEndpoint(FUSEKI_ENDPOINT_EVENT).build()
            is SoccerQuery -> builder.httpClient(httpClient).queryEndpoint(FUSEKI_ENDPOINT_SOCCER).build()
            else -> throw IllegalArgumentException("This QueryType is not Implemented")
        }.use {
            val queryString = type.buildQueryString(parameter, ignoreMissingArgs)
            val query = QueryFactory.create(queryString, Syntax.syntaxSPARQL_11)
            val result = it.query(query).execSelect().asListOfMaps()
            httpClient.close()
            return result
        }
    }
}

fun ResultSet.asListOfMaps(): ArrayList<Map<String, Literal>> {
    val rows = ArrayList<Map<String, Literal>>()

    while (hasNext()) {
        val solution = nextSolution()
        val resultMap = HashMap<String, Literal>()
        solution.varNames().forEach { varName ->
            val literal = solution[varName].asLiteral()
            resultMap[varName] = literal
        }
        rows.add(resultMap)
    }
    return rows
}

fun ResultSet.toJson(): String {
    val outputStream = ByteArrayOutputStream()
    ResultSetFormatter.outputAsJSON(outputStream, this)
    return String(outputStream.toByteArray())
}

interface Query {
    val path: String
    private fun getQueryStringWithPlaceHolder(): String {
        val inputStream = ClassLoader.getSystemResourceAsStream(this.path)
        val queryString = BufferedReader(InputStreamReader(inputStream)).readText()
        inputStream.close()
        return queryString
    }

    fun getRequiredParameterNames(): ArrayList<String> {
        val sparqlTemplate = getQueryStringWithPlaceHolder()
        val parameters = ArrayList<String>()
        Regex("\\{\\{.*}}").findAll(sparqlTemplate).forEach {
            val parameter = it.value.dropLast(2).drop(2)
            if (parameter !in parameters) {
                parameters.add(parameter)
            }
        }
        return parameters
    }

    private fun checkParameters(parameters: Map<String, String>): Boolean {
        val requiredParameters = getRequiredParameterNames()
        if (parameters.isEmpty() && requiredParameters.isNotEmpty()) return false
        requiredParameters.forEach {
            if (!parameters.contains(it) || parameters[it].isNullOrBlank()) return false
        }
        return true
    }

    fun buildQueryString(parameters: Map<String, String>, ignoreMissingArgs: Boolean = false): String {
        var queryString: String
        queryString = if (!checkParameters(parameters)) {
            if (ignoreMissingArgs) filterMissingArgsExpressions(parameters)
            else throw IllegalArgumentException("Missing Parameters for the sparqlQuery. Needed: ${getRequiredParameterNames()}")
        } else {
            getQueryStringWithPlaceHolder()
        }
        parameters.forEach { key, value ->
            queryString = queryString.replace("{{$key}}", value)
        }
        return queryString
    }

    private fun filterMissingArgsExpressions(parameters: Map<String, String>): String {
        val reqParameters = getRequiredParameterNames()
        var queryString = getQueryStringWithPlaceHolder()
        reqParameters.filterNot { parameters.containsKey(it) && parameters[it]!!.isNotBlank() }.forEach {
            queryString = Regex("Filter\\(.*\\{\\{$it}}.*\\)", setOf(RegexOption.IGNORE_CASE)).replace(queryString, "")
        }
        return queryString
    }
}

sealed class EventQuery(override val path: String) : Query {
    object SelectAllArtistNames : EventQuery("sparql/events/queryAllArtistNames.sparql")
    object SelectEventsArtistAndSimilar : EventQuery("sparql/events/queryEventsArtistSimArtist.sparql")
    object SelectAllCityNames : EventQuery("sparql/events/queryAllCityNames.sparql")
    object SelectEvent : EventQuery("sparql/events/queryEvent.sparql")

}

sealed class SoccerQuery(override val path: String) : Query {
    object Answer1 : SoccerQuery("sparql/queryBeer.sparql")
    object Average : SoccerQuery("sparql/average.sparql")
}

