import java.time.Duration

const val MUSIC_BRAINZ_BASE_URL = "http://musicbrainz.org/ws/2/"
const val MUSIC_BRAINZ_LIMIT = 100
val MUSIC_BRAINZ_DELAY = Duration.ofSeconds(1)
const val LASTFM_SCROBBLER_BASE_URL = "http://ws.audioscrobbler.com/"
const val LASTFM_API_KEY = "fb8ea7ed836668e2e17bde127fed24ed"
val LASTFM_DELAY: Duration = Duration.ofSeconds(1 / 5)
const val EVENTFUL_API_KEY = "GxPWC3Z4Pfk6jPSh"
const val EVENTFUL_BASE_URI = "http://api.eventful.com/"
const val DBPEDIA_RES_BASE_URI = "http://dbpedia.org/resource/"
val FUSEKI_BASE_URI = if (System.getenv("FUSEKI_BASE_URI").isNullOrEmpty()) "http://localhost" else System.getenv("FUSEKI_BASE_URI")
val FUSEKI_PORT = if (System.getenv("FUSEKI_PORT").isNullOrEmpty()) "3030" else System.getenv("FUSEKI_PORT")
const val FUSEKI_DATASET_PATH = "\$/datasets"
const val FUSEKI_ENDPOINT_EVENT = "/event/query"
const val FUSEKI_ENDPOINT_SOCCER = "/beer/query"


// 2.0/?method=artist.getsimilar&artist=cher&api_key=YOUR_API_KEY&format=json