import freemarker.cache.ClassTemplateLoader
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.freemarker.FreeMarker
import io.ktor.locations.Location
import io.ktor.locations.Locations
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import routes.*

fun main(args: Array<String>) {
    embeddedServer(Netty,
            port = 8080,
            module = Application::mainModule)
            .start(wait = true)
}

fun Application.mainModule() {
    install(Locations)
    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(Root::class.java.classLoader, "templates")
    }
    routing {
        main()
        staticRoutes()
        soccer()
        events()
        api()
    }
}

@Location("/soccer")
class Soccer

@Location("/events")
class Events

@Location("/api/events/{endpoint}")
data class ApiEvents(val endpoint: String)

@Location("api/event/{id}")
data class Event(val id: String)

@Location("/api/soccer/{endpoint}")
data class SoccerEvents(val endpoint: String)


class Root() {

}