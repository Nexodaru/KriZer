package routes

import FUSEKI_BASE_URI
import FUSEKI_ENDPOINT_SOCCER
import FUSEKI_PORT
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import kotlinx.html.*
import localData.models.Beer
import localData.models.Marriage
import network.eventfulApi.EventfulLoader
import network.lastFMApi.LastFMDBLoader
import network.lastFMApi.appendSimilarArtists
import network.models.Artist
import network.models.SimilarArtist
import network.models.setDataFromJsonFile
import network.models.writeJsonToFile
import network.musicBrainzApi.MusicBrainzDBLoader
import network.openLigaApi.OpenLigaDBLoader
import org.apache.jena.query.QueryExecutionFactory
import org.apache.jena.query.QueryFactory
import org.apache.jena.query.ResultSetFormatter
import org.apache.jena.query.Syntax
import rdf.RdfEvents
import rdf.Rdfbeer
import java.io.File

fun Routing.main() {

    get("/") {
        call.respondText("Hello World!", ContentType.Text.Plain)
    }

    get("/matches") {
        val league = call.request.queryParameters["league"] ?: ""
        val period = call.request.queryParameters["period"] ?: ""
        if (league.isBlank() || period.isBlank()) {
            call.respondText("Missing Queryparameters")
        }
        val matches = OpenLigaDBLoader.listMatches(league, period)
        call.respondHtml {
            head {
                title { +"SockerMatches" }
            }
            body {
                h1 { +"SoccerMatches $league $period" }
                table {
                    if (matches != null) {
                        matches.forEach {
                            tr {
                                +it.toString()
                            }
                        }
                    }
                }
            }
        }
    }

    get("/marriage") {
        val marriageList = Marriage.getMarriageListFromCsv("Eheschliessungen.csv")
        call.respondHtml {
            head {
                title { +"Marriages" }
            }
            body {
                h1 { +"Marriages" }
                table {
                    tr {
                        th { +"startDate" }
                        th { +"endDate" }
                        th { +"state" }
                        th { +"countMarriage" }
                    }

                    marriageList.forEach {
                        tr {
                            th { +it.startDate.toString() }
                            th { +it.endDate.toString() }
                            th { +it.state }
                            th { +"${it.countMarriage}" }
                        }
                    }
                }
            }
        }
    }

    get("/artists") {
        val artistList = MusicBrainzDBLoader.listGermanArtists()
        val path = "src/main/resources/germanArtists"
        artistList.writeJsonToFile(path)

        call.respondText {
            artistList.toString()
        }
    }

    get("/testJson") {
        val artist = Artist("a", "b", "c", "d", "e")
        val artist2 = Artist("a", "b", "c", "d", "e")
        artist.similarArtists!!.add(SimilarArtist(artist.mbID, 2.9))
        artist2.similarArtists!!.add(SimilarArtist(artist.mbID, 2.9))
        val artistList = listOf<Artist>(artist2, artist)

        val path = "src/main/resources/germanArtistsTESSST"
        artistList.writeJsonToFile(path)
        call.respondText {
            artistList.toString()
        }
    }

    get("/similarArtists/{mbid}") {
        val mbid = call.parameters["mbid"]
        if (mbid != null) {
            val similarArtists = LastFMDBLoader.getSimilarArtistsByMIBD(mbid)

            call.respondText {
                similarArtists.toString()
            }
        } else {
            call.respondText { "error" }
        }
    }

    get("/artistsWithSimilarArtists") {
        val path = ClassLoader.getSystemResource("germanArtists").path
        val artists = ArrayList<Artist>().setDataFromJsonFile(path).appendSimilarArtists()

        artists.writeJsonToFile("src/main/resources/germanArtistsWithSimilarTEST")
        call.respondText { artists.toString() }
    }


    get("/beer") {
        val beerList = Beer.getBeerList("Bierdaten.csv")
        call.respondHtml {
            head {
                title { +"Beer" }
            }
            body {
                h1 { +"Beer" }
                table {
                    tr {
                        th { +"startDate" }
                        th { +"endDate" }
                        th { +"state" }
                        th { +"beerConsume" }
                    }
                    beerList.forEach {
                        tr {
                            th { +it.startDate.toString() }
                            th { +it.endDate.toString() }
                            th { +it.state }
                            th { +"${it.beerConsume}" }
                        }
                    }
                }
            }
        }
    }

    get("createBeerRDF") {
        val rdf = Rdfbeer()
        rdf.createRDF()
        rdf.write()
        //rdf.send("mem")
        call.respondText { "Fertsch" }

    }

    get("createRDFEvent") {
        val rdf = RdfEvents()
        rdf.createRDF()
        //rdf.write()
        rdf.send("tdb")
        call.respondText { "Fertsch" }
    }

    get("ArtistsWithEvents") {
        val path = ClassLoader.getSystemResource("germanArtistsWithSimilar").path
        val artists = ArrayList<Artist>().setDataFromJsonFile(path)
        var matches = 0
        val filteredA = artists.filter { it.similarArtists!!.isNotEmpty() }

        filteredA.forEachIndexed { i, artist ->
            if (artist.addEventfulData()) {
                matches++
            }
            println("MatchedEventfulArtists: $matches | $i Gesamt: ${filteredA.size}  ")
        }
        filteredA.writeJsonToFile("src/main/resources/germanArtistsWithEventsTEST")
        call.respondText { filteredA.count { it.events!!.isNotEmpty() }.toString() }
    }

    get("queryBeer") {
        val path = ClassLoader.getSystemResource("sparql/queryBeer.sparql").path
        val queryString = File(path).readText()
        val query = QueryFactory.create(queryString, Syntax.syntaxSPARQL_11)
        val stream = QueryExecutionFactory.sparqlService("$FUSEKI_BASE_URI:$FUSEKI_PORT/$FUSEKI_ENDPOINT_SOCCER", query)
        val result = stream.execSelect()
        ResultSetFormatter.out(result)
        stream.close()
        call.respondText { queryString }
    }

    get("corr") {
        val a = LastFMDBLoader.getArtistNameCorrection("Kinderwahnsinn")
        println(a)
        call.respondText { a.toString() }
    }

    get("event") {
        val a = EventfulLoader.getEvent("P0-001-014195504-6")
        call.respondText { a.toString() }
    }
}