package routes

import io.ktor.content.resources
import io.ktor.content.static
import io.ktor.locations.Location
import io.ktor.routing.Routing

@Location("/static/bootstrap/js/{file}")
data class BootstrapJS(val file: String = "bootstrap.js")

@Location("/static/bootstrap/css/{file}")
data class BootstrapCSS(val file: String = "bootstrap.css")


fun Routing.staticRoutes() {
    static("static") {
        static("bootstrap") {
            resources("bootstrap_4_1_1")
        }
        resources("css")
        resources("images")
        resources("datepicker")
        resources("js")
        resources("mustache")
        resources("templates")
        resources("kotlinJs")
    }
}
