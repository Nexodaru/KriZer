package routes

import Events
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.locations.get
import io.ktor.response.respond
import io.ktor.routing.Routing

private val title = "Events"
private val desc = "Du hast eine Lieblingsband, welche deiner Meinung nach gute Musik macht und hast Lust auf ein Konzert in deiner Stadt?" +
        " Suche hier Events zu deinem Artisten und Events von Artisten, welchem deinem ähneln. Um dies zu ermöglichen wurden alle Deutschen Artisten mit Hilfe der MusicBrainzApi" +
        " abgefragt. Weiterhin wurden diese Daten durch abfragen an die LastFM-APi um ähnliche Artisten erweitert, welche ebenfalls in Deutschland ansässig sind." +
        " Die zu jedem Artisten relevanten Events wurden letztendlich von der Eventful-Api bezogen. Die gewonnenen Daten sind in einem TripleStore (Fuseki)" +
        " im RDF-Format gespeichert und können durch diese Webseite abgefragt werden!"

fun Routing.events() {
    get<Events> {
        call.respond(FreeMarkerContent("Events.ftl", mapOf(
                "title" to title,
                "desc" to desc
        )))
    }
}