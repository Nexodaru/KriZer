package routes

import Soccer
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import localData.models.Beer
import localData.models.Marriage
import org.apache.http.conn.HttpHostConnectException
import org.apache.jena.query.QueryExecutionFactory
import org.apache.jena.query.QueryFactory
import org.apache.jena.query.ResultSetFormatter
import org.apache.jena.query.Syntax
import org.slf4j.LoggerFactory
import rdf.Rdfbeer
import stateList
import templates.TableData
import java.io.File

private val title = "Beer"
private val desc = "Mit Hilfe von www.destatis.de und www.openligadb.de/api wird versucht der Zusammenhang von dem Bierkonsum, Fussball und Eheschließungen darzustellen. Das Projekt ist entstanden im Rahmen des Moduls Semantic Web im SS18."
private val avgTable = createAverageTable()
private val tList = listOf("1. FC Köln", "Borussia Dortmund", "Hertha BSC", "RB Leipzig", "TSG 1899 Hoffenheim")
private val LOG = LoggerFactory.getLogger(Routing::class.java)

fun Routing.soccer() {
    get<Soccer> {

        call.respond(FreeMarkerContent("Beer.ftl", mapOf(
                "title" to title,
                "desc" to desc,
                "data" to avgTable,
                "teamList" to tList
        )))
    }

    post<Soccer> {
        val post = call.receive<Parameters>()
        println(post.entries())

        when (true) {
            post["do"]?.equals("refreshRDF") -> {
                try {
                    Rdfbeer().refreshRDF()
                    call.respond(HttpStatusCode(200, "Ok"), "Refresh Succeed")
                } catch (e: HttpHostConnectException) {
                    call.respond(HttpStatusCode.ServiceUnavailable)
                } finally {
                    return@post
                }
            }
            else -> {
                LOG.info("Nothing to do here")
            }
        }


        call.respond(FreeMarkerContent("Beer.ftl", mapOf(
                "title" to title,
                "desc" to desc,
                "data" to avgTable,
                "teamList" to tList
        )))
    }


}

private fun createAverageTable(): TableData {
    val beerList = Beer.getBeerList("Bierdaten.csv")
    val merrList = Marriage.getMarriageListFromCsv("Eheschliessungen.csv")

    val rows = ArrayList<Pair<String, List<String>>>()
    val head = listOf("Anzahl Eheschließungen", "BierKonsum in hl")

    stateList.forEach { state ->
        val actMerriageData = merrList.filter { it.state == state }
        val actBeerData = beerList.filter { it.state == state }

        val marriageMiddle = (actMerriageData.sumBy { it.countMarriage } / actMerriageData.size).toString()
        val beerMiddle = (actBeerData.sumBy { it.beerConsume } / actBeerData.size).toString()
        rows.add(Pair(state, listOf(marriageMiddle, beerMiddle)))
    }

    val rowArray = Array(rows.size, { _ -> Pair("", emptyList<String>()) })
    rows.toArray(rowArray)

    return TableData("", head, *rowArray)
}