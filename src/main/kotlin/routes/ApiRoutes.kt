package routes

import ApiEvents
import Event
import SoccerEvents
import com.google.gson.GsonBuilder
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.Parameters
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respondText
import io.ktor.routing.Routing
import rdf.EventQuery
import rdf.FusekiSparqlQueryHelper
import rdf.SoccerQuery
import toMapFirst

fun Routing.api() {
    get<ApiEvents> {
        when (it.endpoint) {
            "artistList" -> {
                val result = FusekiSparqlQueryHelper.query(EventQuery.SelectAllArtistNames)
                val stringList = result.map { it["artistName"]?.string }
                call.respondText(GsonBuilder().create().toJson(stringList), ContentType.parse("application/json"))
            }

            "cityList" -> {
                val result = FusekiSparqlQueryHelper.query(EventQuery.SelectAllCityNames)
                println(result)
                val stringList = result.map { it["cityName"]?.string }
                call.respondText(GsonBuilder().create().toJson(stringList), ContentType.parse("application/json"))
            }

            "artistListWithShowableEvents" -> println("ShowE")
            "existsEvent" -> println("ExistsE")
        }
    }

    get<Event> {
        val parameters = mapOf(Pair("eid", it.id))
        val result = FusekiSparqlQueryHelper.query(EventQuery.SelectEvent, parameters)
        val transformedResult = result.map { it.mapValues { it.value.string } }.first()
        call.respondText(GsonBuilder().create().toJson(transformedResult), ContentType.parse("application/json"))
    }

    post<ApiEvents> {
        when (it.endpoint) {
            "ArtistAndSimilarFull" -> {
                val parameters = call.receive<Parameters>().toMapFirst()
                val result = FusekiSparqlQueryHelper.query(EventQuery.SelectEventsArtistAndSimilar, parameters, true)
                val transformedResult = result.map { it.mapValues { it.value.string } }
                call.respondText(GsonBuilder().create().toJson(transformedResult), ContentType.parse("application/json"))
            }
        }
    }

    post<SoccerEvents> {
        when (it.endpoint) {
            "question1" -> {
                val parameters = call.receive<Parameters>().toMapFirst()
                val result = FusekiSparqlQueryHelper.query(SoccerQuery.Answer1, parameters)
                val transformedResult = result.map {
                    it.mapValues {
                        when (it.key) {
                            "state" -> it.value.string
                            "teamname" -> it.value.string
                            else -> it.value.int
                        }
                    }
                }.first()
                call.respondText(GsonBuilder().create().toJson(transformedResult), ContentType.parse("application/json"))
            }

            "average" -> {
                val parameters = call.receive<Parameters>().toMapFirst()
                val result = FusekiSparqlQueryHelper.query(SoccerQuery.Average, parameters)
                println(result)
                val transformedResult = result.map {
                    it.mapValues {
                        when (it.key) {
                            "state" -> it.value.string
                            else -> it.value.int
                        }
                    }
                }.first()

                call.respondText(GsonBuilder().create().toJson(transformedResult), ContentType.parse("application/json"))
            }
        }
    }
}