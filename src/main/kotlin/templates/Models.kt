package templates

class TableData(val leftTopCorner: String, var head: List<String>, vararg rows: Pair<String, List<String>>) {
    var rows: ArrayList<Pair<String, List<String>>> = arrayListOf()

    init {
        rows.forEach {
            if (it.second.size > head.size) throw IllegalArgumentException("Rows sind größer als Head")
            val row = ArrayList(it.second)
            while (head.size > row.size) {
                row.add("")
            }
            this.rows.add(Pair(it.first, row))
        }
    }
}