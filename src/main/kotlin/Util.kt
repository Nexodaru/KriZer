import com.google.gson.reflect.TypeToken
import io.ktor.http.Parameters
import io.ktor.util.toMap
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import org.apache.jena.rdf.model.Literal
import java.util.concurrent.TimeUnit
import kotlin.text.RegexOption.IGNORE_CASE

suspend fun myDelay(seconds: Double){
    delay(1/5, TimeUnit.SECONDS)
}

fun parseMonthWordtoInt(month: String): Int {
    val deRegexes = listOf("januar", "februar", "märz", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "dezember").map { it.toRegex(IGNORE_CASE) }
    val enRegexes = listOf("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december").map { it.toRegex(IGNORE_CASE) }
    operator fun Regex.contains(text: CharSequence): Boolean = this.matches(text)

    return when(month){
        in deRegexes[0], in enRegexes[0] -> 1
        in deRegexes[1], in enRegexes[1] -> 2
        in deRegexes[2], in enRegexes[2] -> 3
        in deRegexes[3], in enRegexes[3] -> 4
        in deRegexes[4], in enRegexes[4] -> 5
        in deRegexes[5], in enRegexes[5] -> 6
        in deRegexes[6], in enRegexes[6] -> 7
        in deRegexes[7], in enRegexes[7] -> 8
        in deRegexes[8], in enRegexes[8] -> 9
        in deRegexes[9], in enRegexes[9] -> 10
        in deRegexes[10], in enRegexes[10] -> 11
        in deRegexes[11], in enRegexes[11] -> 12
        else -> -1
    }
}

fun <T, U> retry(m: U, delay: Long = 0, delayUnit: TimeUnit = TimeUnit.SECONDS, f: (m: U) -> List<T>): List<T> {
    var result = emptyList<T>()
    var successful = false
    var tryCount = 0

    while (!successful && tryCount < 10) {
        try {
            result = f(m)
            successful = true
        } catch (e: Exception) {
            e.javaClass.simpleName
            tryCount++
            runBlocking { delay(delay, delayUnit) }
            println("Retry #$tryCount")
        }
    }
    return result
}
val stateList = listOf<String>("Baden-Württemberg","Bayern","Berlin","Brandenburg","Bremen","Hamburg","Hessen","Mecklenburg-Vorpommern","Niedersachsen","Nordrhein-Westfalen","Rheinland-Pfalz","Saarland","Sachsen","Sachsen-Anhalt","Schleswig-Holstein","Thüringen")

enum class States(val state: String){
    BADENWURTEN("Baden-Württemberg"),
    BAYERN("Bayern"),
    BERLIN("Berlin"),
    BRABU("Brandenburg"),
    BREMEN("Bremen"),
    HAMBURG("Hamburg"),
    HESSEN("Hessen"),
    MECKVO("Mecklenburg-Vorpommern"),
    NISA("Niedersachsen"),
    NOWE("Nordrhein-Westfalen"),
    RPFALZ("Rheinland-Pfalz"),
    SACHSEN("Sachsen"),
    SACHANHA("Sachsen-Anhalt"),
    SCHLEHO("Schleswig-Holstein"),
    THURIN("Schleswig-Holstein")

}

val mapType = object : TypeToken<List<Map<String, Literal>>>() {}.type

fun Parameters.toMapFirst(): Map<String, String> {
    return toMap().mapValues {
        if (it.value.size > 1) println("Just the first entry of ${it.key} was consumed")
        it.value.first()
    }
}