if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'krizer'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'krizer'.");
}
if (typeof this['kotlinx-html-js'] === 'undefined') {
  throw new Error("Error loading module 'krizer'. Its dependency 'kotlinx-html-js' was not found. Please, check whether 'kotlinx-html-js' is loaded prior to 'krizer'.");
}
var krizer = function (_, Kotlin, $module$kotlinx_html_js) {
  'use strict';
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var Unit = Kotlin.kotlin.Unit;
  var equals = Kotlin.equals;
  var createHTML = $module$kotlinx_html_js.kotlinx.html.stream.createHTML_6taknv$;
  var strong = $module$kotlinx_html_js.kotlinx.html.strong_k099i5$;
  var InputType = $module$kotlinx_html_js.kotlinx.html.InputType;
  var input = $module$kotlinx_html_js.kotlinx.html.input_mm0abt$;
  var throwCCE = Kotlin.throwCCE;
  var toShort = Kotlin.toShort;
  var ensureNotNull = Kotlin.ensureNotNull;
  var div = $module$kotlinx_html_js.kotlinx.html.js.div_wkomt5$;
  var append = $module$kotlinx_html_js.kotlinx.html.dom.append_k9bwru$;
  var toString = Kotlin.toString;
  var p = $module$kotlinx_html_js.kotlinx.html.p_7ents2$;
  var addClass = Kotlin.kotlin.dom.addClass_hhb33f$;
  var removeClass = Kotlin.kotlin.dom.removeClass_hhb33f$;
  var Exception_init = Kotlin.kotlin.Exception_init_pdl1vj$;
  var get_create = $module$kotlinx_html_js.kotlinx.html.dom.get_create_4wc2mh$;
  var set_id = $module$kotlinx_html_js.kotlinx.html.set_id_ueiko3$;
  var div_0 = $module$kotlinx_html_js.kotlinx.html.div_ri36nr$;
  function autoComplete$addActive(closure$currentFocus) {
    return function (elements) {
      var tmp$, tmp$_0;
      println('addActiv');
      if (elements == null)
        return;
      removeActive(elements);
      if (closure$currentFocus.v >= elements.length)
        closure$currentFocus.v = 0;
      if (closure$currentFocus.v < 0)
        closure$currentFocus.v = elements.length - 1 | 0;
      (tmp$_0 = (tmp$ = elements[closure$currentFocus.v]) != null ? tmp$.classList : null) != null ? (tmp$_0.add('autocomplete-active'), Unit) : null;
    };
  }
  function autoComplete$lambda$lambda(closure$list, closure$i, closure$value) {
    return function ($receiver) {
      var $receiver_0 = closure$list[closure$i];
      var endIndex = closure$value.length;
      $receiver.unaryPlus_pdl1vz$($receiver_0.substring(0, endIndex));
      return Unit;
    };
  }
  function autoComplete$lambda$lambda_0(closure$list, closure$i) {
    return function ($receiver) {
      $receiver.type = InputType.hidden;
      $receiver.value = closure$list[closure$i];
      return Unit;
    };
  }
  function autoComplete$lambda$lambda_1(closure$b, closure$inputElement) {
    return function (it) {
      var tmp$;
      closure$inputElement.value = (Kotlin.isType(tmp$ = closure$b.getElementsByTagName('input')[0], HTMLInputElement) ? tmp$ : throwCCE()).value;
      closeAllLists();
      return Unit;
    };
  }
  function autoComplete$lambda(closure$inputElement, closure$currentFocus, closure$list) {
    return function (it) {
      var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
      var parentDOMTokenList = (tmp$ = closure$inputElement.parentElement) != null ? tmp$.classList : null;
      var parentClasses = {v: ''};
      tmp$_1 = (tmp$_0 = parentDOMTokenList != null ? parentDOMTokenList.length : null) != null ? tmp$_0 : 0;
      for (var i = 0; i < tmp$_1; i++) {
        var actClass = parentDOMTokenList != null ? parentDOMTokenList[i] : null;
        if (!equals(actClass, 'autocomplete')) {
          if (actClass != null) {
            parentClasses.v += ' ' + actClass;
          }
        }
      }
      var value = closure$inputElement.value;
      closeAllLists();
      if (value.length === 0)
        return;
      closure$currentFocus.v = -1;
      var autoCompleteListDIV = document.createElement('DIV');
      autoCompleteListDIV.setAttribute('id', closure$inputElement.id + 'autocomplete-list');
      autoCompleteListDIV.setAttribute('class', 'autocomplete-items ' + parentClasses.v);
      (tmp$_2 = closure$inputElement.parentNode) != null ? tmp$_2.appendChild(autoCompleteListDIV) : null;
      tmp$_3 = closure$list.length;
      for (var i_0 = 0; i_0 < tmp$_3; i_0++) {
        var $receiver = closure$list[i_0];
        var endIndex = value.length;
        if (equals($receiver.substring(0, endIndex).toUpperCase(), value.toUpperCase())) {
          var b = document.createElement('DIV');
          b.setAttribute('class', parentClasses.v);
          b.innerHTML = strong(createHTML(), void 0, autoComplete$lambda$lambda(closure$list, i_0, value));
          var tmp$_4 = b.innerHTML;
          var $receiver_0 = closure$list[i_0];
          var startIndex = value.length;
          b.innerHTML = tmp$_4 + $receiver_0.substring(startIndex);
          b.innerHTML = b.innerHTML + input(createHTML(), void 0, void 0, void 0, void 0, void 0, autoComplete$lambda$lambda_0(closure$list, i_0));
          b.addEventListener('click', autoComplete$lambda$lambda_1(b, closure$inputElement));
          autoCompleteListDIV.appendChild(b);
        }
      }
      return Unit;
    };
  }
  function autoComplete$lambda_0(closure$inputElement, closure$currentFocus, closure$addActive) {
    return function (event) {
      var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
      var autoCompleteListDIV = Kotlin.isType(tmp$ = document.getElementById(closure$inputElement.id + 'autocomplete-list'), HTMLDivElement) ? tmp$ : null;
      var autoCompleteListElements = autoCompleteListDIV != null ? autoCompleteListDIV.getElementsByTagName('div') : null;
      var keyboardEvent = Kotlin.isType(tmp$_0 = event, KeyboardEvent) ? tmp$_0 : throwCCE();
      switch (keyboardEvent.keyCode) {
        case 40:
          println('40');
          tmp$_1 = closure$currentFocus.v;
          closure$currentFocus.v = tmp$_1 + 1 | 0;
          closure$addActive(autoCompleteListElements);
          break;
        case 38:
          println('38');
          tmp$_2 = closure$currentFocus.v;
          closure$currentFocus.v = tmp$_2 - 1 | 0;
          closure$addActive(autoCompleteListElements);
          break;
        case 13:
          event.preventDefault();
          if (closure$currentFocus.v > -1) {
            (Kotlin.isType(tmp$_3 = autoCompleteListElements != null ? autoCompleteListElements[closure$currentFocus.v] : null, HTMLDivElement) ? tmp$_3 : throwCCE()).click();
          }

          break;
      }
    };
  }
  function autoComplete$lambda_1(event) {
    var tmp$;
    closeAllLists(Kotlin.isType(tmp$ = event.target, HTMLElement) ? tmp$ : throwCCE());
  }
  function autoComplete(inputElement, list) {
    var currentFocus = {v: -1};
    var addActive = autoComplete$addActive(currentFocus);
    inputElement.addEventListener('input', autoComplete$lambda(inputElement, currentFocus, list));
    inputElement.addEventListener('keydown', autoComplete$lambda_0(inputElement, currentFocus, addActive));
    document.addEventListener('click', autoComplete$lambda_1);
  }
  function closeAllLists(element) {
    if (element === void 0)
      element = null;
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4;
    println('closeAll');
    var items = document.getElementsByClassName('autocomplete-items');
    tmp$ = items.length;
    for (var i = 0; i < tmp$; i++) {
      if (!equals(element, Kotlin.isType(tmp$_0 = items[i], HTMLElement) ? tmp$_0 : throwCCE())) {
        tmp$_4 = (tmp$_1 = items[i]) != null ? tmp$_1.parentNode : null;
        tmp$_3 = Kotlin.isType(tmp$_2 = items[i], HTMLElement) ? tmp$_2 : throwCCE();
        tmp$_4 != null ? tmp$_4.removeChild(tmp$_3) : null;
      }
    }
  }
  function removeActive(elements) {
    var tmp$, tmp$_0, tmp$_1;
    println('remove');
    tmp$ = elements.length;
    for (var i = 0; i < tmp$; i++) {
      (tmp$_1 = (tmp$_0 = elements[i]) != null ? tmp$_0.classList : null) != null ? (tmp$_1.remove('autocomplete-active'), Unit) : null;
    }
  }
  var API_EP_ARTIST_LIST;
  var API_EP_CITY_LIST;
  var API_EP_EVENT;
  var API_EP_EVENT_LIST_FULL;
  var API_SOCCER_QUESTION_1;
  var API_SOCCER_AVERAGE;
  var EVENT_CARD_TEMPLATE;
  function setAutoCompleteByGet$lambda(closure$http, closure$id) {
    return function (it) {
      var tmp$;
      var readyState = closure$http.readyState;
      if (readyState === XMLHttpRequest.DONE) {
        if (closure$http.status === toShort(200)) {
          var itemList = JSON.parse(closure$http.responseText);
          autoComplete(Kotlin.isType(tmp$ = document.getElementById(closure$id), HTMLInputElement) ? tmp$ : throwCCE(), itemList);
        }
      }
      return Unit;
    };
  }
  function setAutoCompleteByGet(uri, id) {
    var http = new XMLHttpRequest();
    http.open('get', uri, true);
    http.onreadystatechange = setAutoCompleteByGet$lambda(http, id);
    http.send();
  }
  function search$lambda$lambda(closure$target, closure$template) {
    return function (eventArray, f, f_0) {
      var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
      jQuery('#refreshLoader').remove();
      var events = Kotlin.isArray(tmp$ = eventArray) ? tmp$ : throwCCE();
      if (events.length === 0)
        closure$target != null ? (appendNotFound(closure$target), Unit) : null;
      for (tmp$_0 = 0; tmp$_0 !== events.length; ++tmp$_0) {
        var i = events[tmp$_0];
        tmp$_2 = typeof (tmp$_1 = closure$template) === 'string' ? tmp$_1 : throwCCE();
        tmp$_3 = ensureNotNull(i);
        closure$target != null ? (appendEventCard(closure$target, tmp$_2, tmp$_3), Unit) : null;
      }
      return Unit;
    };
  }
  function search$lambda(closure$data, closure$target) {
    return function (template, textStatus, jqXHR) {
      return jQuery.post(getRootUrl() + API_EP_EVENT_LIST_FULL, closure$data, search$lambda$lambda(closure$target, template), 'json');
    };
  }
  function search() {
    var tmp$, tmp$_0, tmp$_1, tmp$_2;
    var eventForm = jQuery("form[name='eventForm']");
    eventForm.submit();
    if (!eventForm.valid())
      return;
    var target = Kotlin.isType(tmp$ = jQuery('#eventTarget')[0], HTMLDivElement) ? tmp$ : null;
    target != null ? (removeChilds(target), Unit) : null;
    target != null ? (appendLoaderRow(target), Unit) : null;
    var startDateField = jQuery('#inputStartDate');
    var startDate = getDatepickerDate(startDateField);
    var endDateField = jQuery('#inputEndDate');
    var endDate = (tmp$_0 = getDatepickerDate(endDateField)) != null ? endOfDay(tmp$_0) : null;
    var formArray = eventForm.serializeArray();
    replaceEntry(formArray, startDateField.attr('name'), (tmp$_1 = startDate != null ? startDate.toISOString() : null) != null ? tmp$_1 : '');
    replaceEntry(formArray, endDateField.attr('name'), (tmp$_2 = endDate != null ? endDate.toISOString() : null) != null ? tmp$_2 : '');
    var data = jQuery.param(formArray);
    jQuery.get(getRootUrl() + EVENT_CARD_TEMPLATE, search$lambda(data, target), 'text');
  }
  function appendNotFound$lambda$lambda$lambda($receiver) {
    $receiver.unaryPlus_pdl1vz$('Es wurden keine Events gefunden!');
    return Unit;
  }
  function appendNotFound$lambda$lambda(this$) {
    return function ($receiver) {
      div(this$, 'col-auto d-flex mx-auto align-items-center', appendNotFound$lambda$lambda$lambda);
      return Unit;
    };
  }
  function appendNotFound$lambda($receiver) {
    div($receiver, 'row', appendNotFound$lambda$lambda($receiver));
    return Unit;
  }
  function appendNotFound($receiver) {
    append($receiver, appendNotFound$lambda);
  }
  function appendEventCard($receiver, template, event) {
    var rendered = Mustache.render(template, event);
    $receiver.insertAdjacentHTML('beforeend', rendered);
  }
  function main$lambda$lambda(response, f, f_0) {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5, tmp$_6, tmp$_7, tmp$_8, tmp$_9;
    var event = response;
    console.log(event);
    var address = event.adress.toString() + ', ' + event.zip + ' ' + event.city + ', ' + event.state + ', ' + event.country;
    var modal = jQuery('#detailsModal');
    modal.find('.modal-title').text(typeof (tmp$ = event.eventName) === 'string' ? tmp$ : throwCCE());
    modal.find('.modal-body #artist-name').text(typeof (tmp$_0 = event.artistName) === 'string' ? tmp$_0 : throwCCE());
    modal.find('.modal-body #artist-id').text(typeof (tmp$_1 = event.mbid) === 'string' ? tmp$_1 : throwCCE());
    modal.find('.modal-body #artist-type').text(typeof (tmp$_2 = event.artistType) === 'string' ? tmp$_2 : throwCCE());
    modal.find('.modal-body #artist-bio').text(typeof (tmp$_3 = event.bio) === 'string' ? tmp$_3 : throwCCE());
    modal.find('.modal-body #event-name').text(typeof (tmp$_4 = event.eventName) === 'string' ? tmp$_4 : throwCCE());
    modal.find('.modal-body #event-start').text(typeof (tmp$_5 = event.eventStart) === 'string' ? tmp$_5 : throwCCE());
    modal.find('.modal-body #event-desc').text(typeof (tmp$_6 = event.eventDesc) === 'string' ? tmp$_6 : throwCCE());
    modal.find('.modal-body #event-id').text(typeof (tmp$_7 = event.eid) === 'string' ? tmp$_7 : throwCCE());
    modal.find('.modal-body #event-info').text(event.info.toString());
    modal.find('.modal-body #location-name').text(typeof (tmp$_8 = event.venueName) === 'string' ? tmp$_8 : throwCCE());
    modal.find('.modal-body #location-typ').text(typeof (tmp$_9 = event.venueType) === 'string' ? tmp$_9 : throwCCE());
    modal.find('.modal-body #location').text(address);
    return modal.find('.modal-body #location-coords').text(event.lat.toString() + ', ' + event.long);
  }
  function main$lambda(event, args) {
    var tmp$;
    var button = Kotlin.isType(tmp$ = event.relatedTarget, HTMLButtonElement) ? tmp$ : throwCCE();
    var eid = button.dataset['eid'];
    jQuery.get(getRootUrl() + API_EP_EVENT + ('/' + toString(eid)), main$lambda$lambda);
  }
  function main(args) {
    var tmp$;
    var currentPageTag = jQuery("meta[name='tag']").attr('content');
    var navAvatar = Kotlin.isType(tmp$ = document.getElementById('navAvatar'), HTMLImageElement) ? tmp$ : null;
    switch (currentPageTag) {
      case 'soccer':
        navAvatar != null ? (navAvatar.src = '/static/circle_soccer_80.png') : null;
        break;
      case 'events':
        navAvatar != null ? (navAvatar.src = '/static/circle_events_80.png') : null;
        setAutoCompleteByGet(getRootUrl() + API_EP_ARTIST_LIST, 'artistInput');
        setAutoCompleteByGet(getRootUrl() + API_EP_CITY_LIST, 'cityInput');
        jQuery('#detailsModal').on('show.bs.modal', main$lambda);
        break;
    }
  }
  var NoSuchElementException_init = Kotlin.kotlin.NoSuchElementException;
  function answer1$lambda$lambda$lambda$lambda(closure$startDate, closure$endDate, closure$sumMatches, closure$formArray, closure$staat, closure$deltaBeer, closure$deltaMarr) {
    return function ($receiver) {
      $receiver.unaryPlus_pdl1vz$('Im Zeitraum ' + toString(closure$startDate != null ? ddmmyyyy(closure$startDate) : null) + ' bis ' + toString(closure$endDate != null ? ddmmyyyy(closure$endDate) : null) + ' fanden ' + closure$sumMatches);
      var $receiver_0 = closure$formArray;
      var first$result;
      first$break: do {
        var tmp$;
        for (tmp$ = 0; tmp$ !== $receiver_0.length; ++tmp$) {
          var element = $receiver_0[tmp$];
          if (equals(element.name, 'teamName')) {
            first$result = element;
            break first$break;
          }
        }
        throw new NoSuchElementException_init('Array contains no element matching the predicate.');
      }
       while (false);
      $receiver.unaryPlus_pdl1vz$(' Spiele der Mannschaft ' + first$result.value + ' statt. Dabei wurde in deren Heimatbundesstaat ' + toString(closure$staat));
      $receiver.unaryPlus_pdl1vz$(' im Monat durchschnittlich ' + closure$deltaBeer + ' hl ' + (closure$deltaBeer < 0 ? 'weniger' : 'mehr') + ' Bier getrunken');
      $receiver.unaryPlus_pdl1vz$(' und ' + closure$deltaMarr + ' mal ' + (closure$deltaMarr < 0 ? 'weniger' : 'mehr') + ' geheiratet!');
      return Unit;
    };
  }
  function answer1$lambda$lambda$lambda(closure$startDate, closure$endDate, closure$sumMatches, closure$formArray, closure$staat, closure$deltaBeer, closure$deltaMarr) {
    return function ($receiver) {
      p($receiver, void 0, answer1$lambda$lambda$lambda$lambda(closure$startDate, closure$endDate, closure$sumMatches, closure$formArray, closure$staat, closure$deltaBeer, closure$deltaMarr));
      return Unit;
    };
  }
  function answer1$lambda$lambda(closure$sumBeer, closure$sumMarriage, closure$responseWrapper, closure$startDate, closure$endDate, closure$sumMatches, closure$formArray, closure$staat) {
    return function (answerAverage, f, f_0) {
      var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3;
      var avgBeer = (tmp$_0 = typeof (tmp$ = answerAverage['avBeer']) === 'number' ? tmp$ : null) != null ? tmp$_0 : 0;
      var avgMarr = (tmp$_2 = typeof (tmp$_1 = answerAverage['avMarriage']) === 'number' ? tmp$_1 : null) != null ? tmp$_2 : 0;
      var deltaBeer = avgBeer - closure$sumBeer | 0;
      var deltaMarr = avgMarr - closure$sumMarriage | 0;
      return (tmp$_3 = closure$responseWrapper != null ? append(closure$responseWrapper, answer1$lambda$lambda$lambda(closure$startDate, closure$endDate, closure$sumMatches, closure$formArray, closure$staat, deltaBeer, deltaMarr)) : null) != null ? tmp$_3 : '';
    };
  }
  function answer1$lambda(closure$responseWrapper, closure$startDate, closure$endDate, closure$formArray) {
    return function (answer, f, f_0) {
      var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5;
      console.log(answer);
      var sumBeer = (tmp$_0 = typeof (tmp$ = answer['sumBeer']) === 'number' ? tmp$ : null) != null ? tmp$_0 : 0;
      var sumMarriage = (tmp$_2 = typeof (tmp$_1 = answer['sumMarriage']) === 'number' ? tmp$_1 : null) != null ? tmp$_2 : 0;
      var sumMatches = (tmp$_4 = typeof (tmp$_3 = answer['sumMatches']) === 'number' ? tmp$_3 : null) != null ? tmp$_4 : 0;
      var staat = typeof (tmp$_5 = answer['state']) === 'string' ? tmp$_5 : null;
      return jQuery.post(getRootUrl() + API_SOCCER_AVERAGE, 'statedb=' + toString(staat), answer1$lambda$lambda(sumBeer, sumMarriage, closure$responseWrapper, closure$startDate, closure$endDate, sumMatches, closure$formArray, staat));
    };
  }
  function answer1() {
    var tmp$, tmp$_0, tmp$_1;
    var form = jQuery("form[name='question1']");
    form.submit();
    if (!form.valid())
      return;
    var responseWrapper = Kotlin.isType(tmp$ = jQuery('#responseWrapper1')[0], HTMLDivElement) ? tmp$ : null;
    responseWrapper != null ? (removeChilds(responseWrapper), Unit) : null;
    var formArray = form.serializeArray();
    var startDate = getDatepickerDate(form.find("input[name='startDate']"));
    var endDate = getDatepickerDate(form.find("input[name='endDate']"));
    replaceEntry(formArray, 'startDate', (tmp$_0 = startDate != null ? startDate.toISOString() : null) != null ? tmp$_0 : '');
    replaceEntry(formArray, 'endDate', (tmp$_1 = endDate != null ? endDate.toISOString() : null) != null ? tmp$_1 : '');
    var formdata = jQuery.param(formArray);
    jQuery.post(getRootUrl() + API_SOCCER_QUESTION_1, formdata, answer1$lambda(responseWrapper, startDate, endDate, formArray));
  }
  function updateDatasets$lambda(closure$http, closure$loader) {
    return function (it) {
      var readyState = closure$http.readyState;
      if (readyState === XMLHttpRequest.DONE) {
        if (closure$http.status === toShort(200)) {
          closure$loader != null ? removeClass(closure$loader, ['loader']) : null;
          closure$loader != null ? addClass(closure$loader, ['fas fa-check fa-2x']) : null;
        }
         else {
          closure$loader != null ? removeClass(closure$loader, ['loader']) : null;
          closure$loader != null ? addClass(closure$loader, ['fas fa-times fa-2x']) : null;
          window.alert('Der TripleStore scheint gerade nicht verf\xFCgbar zu sein!\nVersuche es sp\xE4ter nocheinmal.');
          throw Exception_init('');
        }
      }
      return Unit;
    };
  }
  function updateDatasets() {
    var loader = document.getElementById('refreshLoader');
    loader != null ? (loader.className = '') : null;
    loader != null ? addClass(loader, ['loader']) : null;
    loader != null ? (loader.setAttribute('style', 'visibility: visible'), Unit) : null;
    var http = new XMLHttpRequest();
    http.open('post', window.location.href, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.onreadystatechange = updateDatasets$lambda(http, loader);
    http.send('do=refreshRDF');
  }
  function getRootUrl() {
    var tmp$;
    return typeof (tmp$ = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/') === 'string' ? tmp$ : throwCCE();
  }
  function appendLoaderRow$lambda$lambda$lambda($receiver) {
    set_id($receiver, 'refreshLoader');
    var $receiver_0 = $receiver.attributes;
    var value = 'visibility: visible';
    $receiver_0.put_xwzc9p$('style', value);
    return Unit;
  }
  function appendLoaderRow$lambda$lambda($receiver) {
    div_0($receiver, 'loader', appendLoaderRow$lambda$lambda$lambda);
    return Unit;
  }
  function appendLoaderRow$lambda($receiver) {
    div_0($receiver, 'col-auto d-flex mx-auto align-items-center', appendLoaderRow$lambda$lambda);
    return Unit;
  }
  function appendLoaderRow($receiver) {
    var loaderRow = div(get_create(document), 'row', appendLoaderRow$lambda);
    $receiver.append(loaderRow);
  }
  function removeChilds($receiver) {
    var tmp$;
    while ($receiver.lastChild != null) {
      $receiver.removeChild(Kotlin.isType(tmp$ = $receiver.lastChild, Node) ? tmp$ : throwCCE());
    }
  }
  function replaceEntry($receiver, name, value) {
    var tmp$, tmp$_0;
    var index = 0;
    for (tmp$ = 0; tmp$ !== $receiver.length; ++tmp$) {
      var item = $receiver[tmp$];
      var i = (tmp$_0 = index, index = tmp$_0 + 1 | 0, tmp$_0);
      if (equals(item.name, name))
        $receiver[i].value = value;
    }
  }
  function getDatepickerDate($receiver) {
    var tmp$;
    return Kotlin.isType(tmp$ = $receiver.datepicker('getUTCDate'), Date) ? tmp$ : null;
  }
  function endOfDay($receiver) {
    return new Date($receiver.getUTCFullYear(), $receiver.getUTCMonth(), $receiver.getUTCDay(), 25, 59, 59);
  }
  function ddmmyyyy($receiver) {
    return $receiver.getUTCDate().toString() + '/' + ($receiver.getUTCMonth() + 1 | 0) + '/' + $receiver.getUTCFullYear();
  }
  _.autoComplete_h1vrdu$ = autoComplete;
  Object.defineProperty(_, 'API_EP_ARTIST_LIST', {
    get: function () {
      return API_EP_ARTIST_LIST;
    }
  });
  Object.defineProperty(_, 'API_EP_CITY_LIST', {
    get: function () {
      return API_EP_CITY_LIST;
    }
  });
  Object.defineProperty(_, 'API_EP_EVENT', {
    get: function () {
      return API_EP_EVENT;
    }
  });
  Object.defineProperty(_, 'API_EP_EVENT_LIST_FULL', {
    get: function () {
      return API_EP_EVENT_LIST_FULL;
    }
  });
  Object.defineProperty(_, 'API_SOCCER_QUESTION_1', {
    get: function () {
      return API_SOCCER_QUESTION_1;
    }
  });
  Object.defineProperty(_, 'API_SOCCER_AVERAGE', {
    get: function () {
      return API_SOCCER_AVERAGE;
    }
  });
  Object.defineProperty(_, 'EVENT_CARD_TEMPLATE', {
    get: function () {
      return EVENT_CARD_TEMPLATE;
    }
  });
  var package$events = _.events || (_.events = {});
  package$events.setAutoCompleteByGet_puj7f4$ = setAutoCompleteByGet;
  package$events.search = search;
  _.main_kand9s$ = main;
  var package$soccer = _.soccer || (_.soccer = {});
  package$soccer.answer1 = answer1;
  package$soccer.updateDatasets = updateDatasets;
  _.getRootUrl = getRootUrl;
  _.appendLoaderRow_8igmqs$ = appendLoaderRow;
  _.removeChilds_y4uc6z$ = removeChilds;
  _.replaceEntry_w9zsdm$ = replaceEntry;
  _.getDatepickerDate_a5lmlr$ = getDatepickerDate;
  _.endOfDay_t5kl13$ = endOfDay;
  _.ddmmyyyy_t5kl13$ = ddmmyyyy;
  API_EP_ARTIST_LIST = 'api/events/artistList';
  API_EP_CITY_LIST = 'api/events/cityList';
  API_EP_EVENT = 'api/event';
  API_EP_EVENT_LIST_FULL = 'api/events/ArtistAndSimilarFull';
  API_SOCCER_QUESTION_1 = 'api/soccer/question1';
  API_SOCCER_AVERAGE = 'api/soccer/average';
  EVENT_CARD_TEMPLATE = 'static/eventCard.mst';
  main([]);
  Kotlin.defineModule('krizer', _);
  return _;
}(typeof krizer === 'undefined' ? {} : krizer, kotlin, this['kotlinx-html-js']);

//# sourceMappingURL=krizer.js.map
