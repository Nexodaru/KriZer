<#-- @ftlvariable name="teamList" type="kotlin.collections.List" -->
<#-- @ftlvariable name="desc" type="java.lang.String" -->
<#-- @ftlvariable name="title" type="java.lang.String" -->
<#-- @ftlvariable name="data" type="templates.TableData" -->

<#import "Base.ftl" as layout />
<#import "Snippets.ftl" as snippets />

<#assign head>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</#assign>

<#assign script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
    <script>
        var options = {
            language: "de-DE",
            format: "MM/yyyy",
            startView: 1,
            minViewMode: 1,
            maxViewMode: 2,
            immediateUpdates: true
        };
        $('.input-daterange input').each(function () {
            $(this).datepicker(options);
        });
    </script>
</#assign>

<@layout.baseTemplate title = "${title}" head=head script=script tag="soccer">
    <@snippets.jumbotron title="${title}" desc="${desc}"></@snippets.jumbotron>

        <div class="row mb-4">
            <div class="col"></div>
            <div class="col">
                <button type="button" class="btn btn-dark" onclick="krizer.soccer.updateDatasets()"
                        style="width: 100%">Update Datasets
                </button>
            </div>
            <div class="col d-flex align-items-center">
                <div id="refreshLoader" class="loader"></div>
            </div>
        </div>

        <div class="row mb-2">
            <div class="col">
                <@snippets.collapseAbleCard title="Statistik Ø Eheschließungen &
                    Bierkonsum je Bundesland">
                    <@snippets.table></@snippets.table>
                    <div class="mb-2 text-muted small">Die Daten beziehen sich auf den monatlichen
                        Durchschnitt.
                    </div>
                </@snippets.collapseAbleCard>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start" style="overflow: auto">
                        <strong class="row d-inline-block mb-4 text-primary">Wie viel Europa League Spiele finden in dem
                            Zeitraum X bis Y vom Team Z statt? Werden während der Spiele mehr Ehen geschlossen
                            / Bier konsumiert als im statistischen Mittel?
                        </strong>
                        <form name="question1" class="form-inline align-self-center">
                            <@snippets.dropdown classes="mb-3" id="teamName" mylist=teamList></@snippets.dropdown>
                            <div class="input-group input-daterange mb-3">
                                <input name="startDate" type="text" class="form-control" placeholder="StartDate"
                                       required>
                                <input name="endDate" type="text" class="form-control" placeholder="EndDate" required>
                            </div>
                            <div class="col mb-3 text-center" style="width: 100%">
                                <button id="submit" type="button" onclick="krizer.soccer.answer1()"
                                        class="btn btn-dark">Submit
                                </button>
                            </div>
                        </form>
                        <div id="responseWrapper1"></div>
                    </div>
                </div>
            </div>
        </div>
</@layout.baseTemplate>