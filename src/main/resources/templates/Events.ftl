<#-- @ftlvariable name="desc" type="java.lang.String" -->
<#-- @ftlvariable name="title" type="java.lang.String" -->

<#import "Base.ftl" as layout />
<#import "Snippets.ftl" as snippets />

<#assign head>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
          integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</#assign>

<#assign script>
    <script type="text/javascript"
            src="/static/mustache.js"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        var options = {
            language: "de-DE",
            format: "dd-mm-yyyy",
            startView: 0,
            minViewMode: 0,
            maxViewMode: 2,
            immediateUpdates: true
        };
        $('#inputEndDate').datepicker(options);
        $('#inputStartDate').datepicker(options);
    </script>
</#assign>

<@layout.baseTemplate script=script head=head title = "${title}" tag="events">
    <@snippets.jumbotron title="${title}" desc="${desc}"></@snippets.jumbotron>

    <form autocomplete="off" class="mb-5" name="eventForm">
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="autocomplete">
                    <label for="artistInput">Artist</label>
                    <input class="autocompleteInput" id="artistInput" type="text" name="artistName"
                           placeholder="Artists" required>
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="autocomplete">
                    <label for="cityInput">Stadt</label>
                    <input class="autocompleteInput" id="cityInput" type="text" name="city" placeholder="Stadt">
                </div>
            </div>

            <div class="form-group col-md-6">
                <label for="inputStartDate">Von</label>
                <input id="inputStartDate" name="minDate" type="text" class="form-control"
                       placeholder="StartDate">
            </div>
            <div class="form-group col-md-6">
                <label for="inputEndDate">Bis</label>
                <input name="maxDate" id="inputEndDate" type="text" class="form-control" placeholder="EndDate">
            </div>
        </div>
        <div class="form-row">
            <div class="col-12 text-center mt-2">
                <button id="submit" type="button" onclick="krizer.events.search()"
                        class="btn btn-dark">Submit
                </button>
            </div>
        </div>
    </form>

    <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detailsModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <h2>Artist</h2>
                        <div class="form-group">
                            <label for="artist-name" class="col-form-label">Name</label>
                            <p type="text" class="form-control" id="artist-name">
                        </div>
                        <div class="form-group">
                            <label for="artist-id" class="col-form-label">MusicBrainz-ID</label>
                            <p class="form-control" id="artist-id"></p>
                        </div>
                        <div class="form-group">
                            <label for="artist-type" class="col-form-label">Typ</label>
                            <p class="form-control" id="artist-type"></p>
                        </div>
                        <div class="form-group">
                            <label for="artist-bio" class="col-form-label">Biographie</label>
                            <p class="form-control" id="artist-bio"></p>
                        </div>
                        <h2>Event</h2>
                        <div class="form-group">
                            <label for="event-name" class="col-form-label">Name</label>
                            <p type="text" class="form-control" id="event-name">
                        </div>
                        <div class="form-group">
                            <label for="event-start" class="col-form-label">Beginn</label>
                            <p class="form-control" id="event-start"></p>
                        </div>
                        <div class="form-group">
                            <label for="event-desc" class="col-form-label">Beschreibung</label>
                            <p class="form-control" id="event-desc"></p>
                        </div>
                        <div class="form-group">
                            <label for="event-id" class="col-form-label">Eventful-ID</label>
                            <p class="form-control" id="event-id"></p>
                        </div>
                        <div class="form-group">
                            <label for="event-info" class="col-form-label">Weitere Infos</label>
                            <p class="form-control" id="event-info"></p>
                        </div>
                        <h2>Location</h2>
                        <div class="form-group">
                            <label for="location-name" class="col-form-label">Name</label>
                            <p type="text" class="form-control" id="location-name">
                        </div>
                        <div class="form-group">
                            <label for="location-typ" class="col-form-label">Art</label>
                            <p class="form-control" id="location-typ"></p>
                        </div>
                        <div class="form-group">
                            <label for="location" class="col-form-label">Adresse</label>
                            <p class="form-control" id="location"></p>
                        </div>
                        <div class="form-group">
                            <label for=location-coords" class="col-form-label">Koordinaten</label>
                            <p class="form-control" id="location-coords"></p>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="eventTarget"></div>

</@layout.baseTemplate>
