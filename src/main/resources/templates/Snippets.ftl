<#-- @ftlvariable name="data" type="templates.TableData" -->

<#macro jumbotron title = "Titel" desc="Beschreibung hier rein">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">${title}</h1>
            <p class="lead text-muted">${desc} </p>
        </div>
    </section>
</#macro>

<#macro table>
<div class="table-responsive">
    <table class="table my-2 table-striped table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>${data.leftTopCorner}</th>
        <#list data.head as head>
            <th scope="col">${head}</th>
        </#list>
        </tr>
        </thead>
        <tbody>
        <#list data.rows as row>
        <tr>
            <th>${row.first}</th>
            <#list row.second as col>
                <td>${col}</td>
            </#list>
        </tr>
        </#list>
        </tbody>
    </table>
</div>
</#macro>

<#macro dropdown mylist classes="" id="id" label="" >
   <div class="form-group ${classes}">
       <label for="${id}">${label}</label>
       <select class="form-control" name="${id}" id="${id}">
           <#list mylist as item>
               <option>${item}</option>
           </#list>
       </select>
   </div>
</#macro>

<#macro collapseAbleCard title="Titel hier eingeben">
    <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start" style="overflow: auto">
            <div class="row" style="width: 100%;">
                <div class="col-10">
                    <strong class="d-inline-block mb-2 text-primary">${title}</strong>
                    <!--<div class="mb-1 text-muted">Nov 12</div>-->
                </div>
                <div class="col-2 align-items-center text-right">
                    <button class="btn btn-dark" type="button" data-toggle="collapse"
                            data-target="#collapseElement" aria-expanded="false"
                            aria-controls="collapseExample">
                        -
                    </button>
                </div>
            </div>
            <div class="collapse" style="width: 100%" id="collapseElement">
                <#nested>
            </div>
        </div>
    </div>
</#macro>