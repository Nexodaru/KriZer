<#-- @ftlvariable name="title" type="java.lang.String" -->

<#macro baseTemplate script="" head="" title="title" tag="">
<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="tag" content="${tag}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/static/krizer.css">
    ${head}
</head>
<body>

<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="collapse navbar-collapse w-100 dual-collapse2 order-1 order-md-0" id="navbarCollapse">
            <ul class="navbar-nav ml-auto text-center">
                <li class="nav-item navbar-center">
                    <a id="soccer" class="nav-link" href="/soccer">Soccer</a>
                </li>
            </ul>
        </div>
        <div class="mx-auto order-0 order-md-1 position-relative">
            <a class="mx-auto" href="#">
                <img id="navAvatar" src="" class="rounded-circle mr-4 ml-4">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse w-100 dual-collapse2 order-2 order-md-2">
            <ul class="navbar-nav mr-auto text-center">
                <li class="nav-item">
                    <a id="events" class="nav-link" href="/events">Events</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<main role="main" class="Content container">
    <title>${title}</title>
    <#nested />
</main>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="/static/bootstrap/js/bootstrap.js"></script>
<script src="/static/kotlin.js"></script>
<script src="/static/kotlinx-html-js.js"></script>
<script src="/static/krizer.js"></script>
    ${script}
</body>
</html>
</#macro>